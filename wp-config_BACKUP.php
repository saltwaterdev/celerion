<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'celerion');

if (strpos($_SERVER['SERVER_NAME'],'saltwaterlab') !== false) {
	define('DB_USER', 'celerion_usr');
	define('DB_PASSWORD', 'oIq7w%93');
} else {
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
}

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_>.bvGpx_/9{`8,WXC_6li^Z,QrR*Ia+9m~m~MLmEnA2e,_2ib|}7ESJx>?nd/W~');
define('SECURE_AUTH_KEY',  '7S-4a[NRKdR@2(X72OgA9fK&nUzvg?$60MZq<W4Cur++9a@ee{ss-dr(!x(Bp{Ok');
define('LOGGED_IN_KEY',    '[S|Bi=Ji|D[x}|:y6//*JHxLFqS&x E9?h]N^;_drt(mvc_P*Y7Ysam)C.dek(m?');
define('NONCE_KEY',        '?oGw2BslOQ-u=gI5Sctclh%IhagpGG-1Q5C%-mq3I-que]$qA/s8YWBnUIB7XJKD');
define('AUTH_SALT',        'w1)/$oE,P-x21yK<2DH=R9P[=FhlI2;=K%fm=qebyR1^f`%f{?t/qZEbpQaAHz@&');
define('SECURE_AUTH_SALT', ':lFxFyuiyz0C%0Aakwtt[n8e~ zprGD$Y9+GZq`Jj%3#a;uX+$_RWzjx<1GwU2.,');
define('LOGGED_IN_SALT',   '-R;j%QDrpGQcC:AAhQSj<b$EbN-%m-X*DjcYHU`XOeL.9UnEp9iaD@f}txm#-O&6');
define('NONCE_SALT',       'n+v%#Zy-gY((:(s6U%>|@F.C0Q%4ML7D=!$_6vq?Kh2lDFhZZs]qn:Wi>v$Q&SB~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define( 'WP_ALLOW_MULTISITE', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
