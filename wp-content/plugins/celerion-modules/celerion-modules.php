<?php

/**
 * Plugin Name: Celerion Modules
 * Plugin URI: http://www.celerion.com
 * Description: Custom modules for the Celerion
 * Version: 1.0
 * Author: Alex Davis
 * Author URI: https://www.saltwaterco.com
 */
define( 'CELERION_DIR', plugin_dir_path( __FILE__ ) );
define( 'CELERION_URL', plugins_url( '/', __FILE__ ) );

function celerion_modules_examples() {
    if ( class_exists( 'FLBuilder' ) ) {
        require_once 'hero-module/hero-module.php';
        require_once 'cta-box/cta-box.php';
        require_once 'contact-box/contact-box.php';
        require_once 'contact-box_two/contact-box-two.php';
        require_once 'image-and-cta/image-and-cta.php';
        require_once 'autonav/autonav.php';
        require_once 'icon-box/icon-box.php';
        require_once 'accordion/accordion.php';
        require_once 'page-list/page-list.php';
         require_once 'news-list/news-list.php';
        require_once 'preview-content/preview-content.php';
        require_once 'spotlight/spotlight.php';
    }
}
add_action( 'init', 'celerion_modules_examples' );