.fl-node-<?php echo $id; ?> {
    background-color: #232827;
    border-radius: 5px;
    padding: 6.15em 0;
    margin-bottom: 5em;
}

.fl-node-<?php echo $id; ?> .content{
	width: 91%;
	margin: 0 auto;
	overflow: hidden;
}

.fl-node-<?php echo $id; ?> .left{
	width: 100%;
	float: left;
	border-top: 1px solid white;
	padding-top: 2.25em;
}

.fl-node-<?php echo $id; ?> .right{
	width: 100%;
	float: right;
	padding-top: 1.25em;
}

.fl-node-<?php echo $id; ?> .subtitle{
	color: white;
	width: 100%;
	display: block;
	font-family:'Helvetica Neue LT W01_75 Bold';
	font-size: 1.17em;
	margin: 0;
	padding-bottom: 1.1em;
}

.fl-node-<?php echo $id; ?> .subtitle span{
	background-color: #b1005d;
	width: 0.844em;
    height: 0.844em;
    display: inline-block;
    margin-right: 0.8em;
}

.fl-node-<?php echo $id; ?> .left h4{
	font-family: 'Helvetica Neue LT W01_75 Bold';
    color: white;
    font-weight: 400;
    font-size: 3.05em;
    margin: 0;
    line-height: 1em;
}

.fl-node-<?php echo $id; ?> .right p{
	color: white;
	margin: 0;
	margin-bottom: 2em;
}

.fl-node-<?php echo $id; ?> .right p:last-child{
	margin-bottom: 0;
}

.fl-node-<?php echo $id; ?> .right p a span.button{
	padding: 0.577em 2.47em;
	border: 2px solid white;
}

.fl-node-<?php echo $id; ?> .right p a:hover span.button{
    background-color: #232827;
    color: white;
    border: 2px solid white;
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> .left{
		width: 39%;
	}
	
	.fl-node-<?php echo $id; ?> .right{
		width: 59%;
		border-top: 1px solid white;
		padding-top: 1.25em;
	}
}