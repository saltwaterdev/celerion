<div class="content">
	<?php if ($settings->subtitle){
		$subtitle = $settings->subtitle;
	} else {
		$subtitle = "CONTACT";
	} ?>
	<p class="subtitle"><span></span><?php echo $subtitle; ?></p>
	<div class="left">
		<h4><?php echo $settings->title; ?></h4>
	</div>
	<div class="right">
		<?php echo $settings->content; ?>
	</div>
</div>