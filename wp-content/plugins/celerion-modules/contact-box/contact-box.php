<?php

class ContactBoxClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Contact Box', 'fl-builder' ),
            'description'     => __( 'Build a content box', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'contact-box/',
            'url'             => CELERION_URL . 'contact-box/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'ContactBoxClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Contact Box Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'Largest text displayed on the left side', 'fl-builder' ),
					    'help'          => __( 'This text will be the most prominent item in the box.', 'fl-builder' )
					),
					'subtitle' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Subtitle', 'fl-builder' ),
					    'class'         => 'subtitle',
					    'description'   => __( 'If left blank, will default to Contact', 'fl-builder' ),
					    'help'          => __( 'This text will be the shown above the title', 'fl-builder' )
					),
                    'content' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
                )
            )
        )
    )
) );