<?php

class AccordionClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Accordion', 'fl-builder' ),
            'description'     => __( 'Build an expanding and collapsing accordion', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'accordion/',
            'url'             => CELERION_URL . 'accordion/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'AccordionClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Accordion Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Text Field', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'This will be what shows and what lets people know to click to expand or collapse.', 'fl-builder' ),
					    'help'          => __( 'This text will be the most prominent item in the box.', 'fl-builder' )
					),
                    'content' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
                )
            )
        )
    )
) );