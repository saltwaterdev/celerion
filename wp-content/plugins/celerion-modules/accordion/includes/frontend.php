<div class="content accordion">
	<a href="#" class="accordion_trigger"><?php echo $settings->title; ?> <i class="far fa-chevron-circle-down"></i></a>
	<div class="holder hidden">
		<div class="contents">
			<?php echo $settings->content; ?>
		</div>
	</div>
</div>