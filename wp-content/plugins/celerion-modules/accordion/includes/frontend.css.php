.fl-node-<?php echo $id; ?> {
	margin-bottom: 1em; 
}

.fl-node-<?php echo $id; ?> a {
	display: block;
    width: 100%;
    color: white;
    background-color: #232827;
    font-weight: bold;
    font-size: 1.25em;
    padding: 0.5em 1em;
}

.fl-node-<?php echo $id; ?> a i{
	float: right;
    position: relative;
    top: 0.15em;
    -moz-transition: transform 0.15s linear;
	-webkit-transition: transform 0.15s linear;
	-o-transition-property: transform 0.15s linear;
	-ms-transition-property: transform 0.15s linear;
	transition-property: transform 0.15s linear;
}

.fl-node-<?php echo $id; ?> a.active i{
	transform: rotate(180deg); /* Equal to rotateZ(45deg) */
}

.fl-node-<?php echo $id; ?> .holder{
	border: 2px solid #232827;
    width: 100%;
}

.fl-node-<?php echo $id; ?> .holder.hidden{
	display: none;	
}

.fl-node-<?php echo $id; ?> .holder .contents{
	padding: 0.5em 1.4em;
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?>{
		
	}
}