<?php

class PreviewContentClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Preview Content', 'fl-builder' ),
            'description'     => __( 'Build a content block with a preview expander, including photo', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'preview-content/',
            'url'             => CELERION_URL . 'preview-content/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'PreviewContentClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Preview Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'pic' => array(
					    'type'          => 'photo',
					    'label'         => __('Optional Photo', 'fl-builder'),
					    'show_remove'   => false,
					),
					'title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'If left blank, no subtitle will show', 'fl-builder' ),
					    'help'          => __( 'The largest piece of text on the preview box.', 'fl-builder' )
					),
					'subtitle' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Subtitle', 'fl-builder' ),
					    'class'         => 'subtitle',
					    'description'   => __( 'If left blank, no subtitle will show', 'fl-builder' ),
					    'help'          => __( 'Subtitle displaying directly under the title.', 'fl-builder' )
					),
                    'content' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'content_position' => array(
					    'type'          => 'select',
					    'label'         => __( 'Content Position', 'fl-builder' ),
					    'default'       => 'left',
					    'options'       => array(
					        'left'      => __( 'Left', 'fl-builder' ),
					        'right'      => __( 'Right', 'fl-builder' )
					    )
					),
                )
            )
        )
    )
) );