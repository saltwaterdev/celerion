.fl-node-<?php echo $id; ?> {
    overflow: hidden;
    position: relative;
}

.fl-node-<?php echo $id; ?> .preview_content{
	overflow: hidden;
	margin-bottom: 5em;
}

.fl-node-<?php echo $id; ?> img.preview_pic{
	max-width: 150px;
	height: auto;
	width: 150px;
	margin-right: 17px;
	float: left;
}

.fl-node-<?php echo $id; ?> .copy{
	border-top: 1px solid #232827;
	width: calc(100% - 167px);
	float: left;
	height: 289px;
	overflow: hidden;
	padding: 0.9em 0;
	position: relative;
}

.fl-node-<?php echo $id; ?> h4{
	font-size: 1em;
	margin-top: 0.4em;
}

.fl-node-<?php echo $id; ?> .copy:after{
	content: "";
	position: absolute;
	left: 0;
	bottom: 0;
	width: 100%;
	height: 45px;
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
}

.fl-node-<?php echo $id; ?> a.more{
	border-top: 1px solid #232827;
    width: calc(100% - 167px);
    text-align: center;
    margin-top: 0;
    display: block;
    padding-top: 1.25em;	
    margin-left: 150px;
    float: right;
    font-family: 'Helvetica Neue LT W01_75 Bold';
    font-size: 1.13em;
}

.fl-node-<?php echo $id; ?> a.more span{
	position: relative;
	display: inline-block;
}

.fl-node-<?php echo $id; ?> a.more span:after{
	content: "";
	position: absolute;
	bottom: 0;
	left: 0;
	width: 0%;
	height: 1px;
	background-color: #232827;
	-moz-transition: width 0.15s linear;
	-webkit-transition: width 0.15s linear;
	-o-transition-property: width 0.15s linear;
	-ms-transition-property: width 0.15s linear;
	transition-property: width 0.15s linear;
}

.fl-node-<?php echo $id; ?> a.more:hover span:after{
	width: 100%;
}

.fl-node-<?php echo $id; ?> a.more i{
	color: #b30560;
    margin-left: 0.3em;
    -moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> a.more.open i{
	transform: rotate(180deg);
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> img.preview_pic{
		max-width: 230px;
		width: 230px;
		margin-right: 35px;
	}
	.fl-node-<?php echo $id; ?> .copy{
		width: calc(100% - 265px);
	}
	.fl-node-<?php echo $id; ?> a.more{
		width: calc(100% - 265px);
		margin-left: 230px;
	}
}