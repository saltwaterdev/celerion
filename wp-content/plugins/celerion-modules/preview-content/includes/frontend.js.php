jQuery(document).ready(function($) {
	$(".fl-node-<?php echo $id; ?> a.more").click(function(e) {
		e.preventDefault();
		elem = $(this).siblings(".copy");
		elem.toggleClass("open");
		$(this).toggleClass("open");
		if (elem.hasClass("open")){
			elem.animate({height: elem.get(0).scrollHeight}, 500 );
			$(this).children("span").text("Collapse");
		} else {
			elem.animate({height: 289}, 500 );
			$(this).children("span").text("Show More");
		}
	});
});