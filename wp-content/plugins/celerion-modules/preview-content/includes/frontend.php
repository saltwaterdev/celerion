<div class="preview_content">
	<?php if ($settings->pic){ ?>
		<img src="<?php echo wp_get_attachment_url($settings->pic); ?>" class="preview_pic">
	<?php } ?>
	<div class="copy">
		<?php if ($settings->title){ echo '<h3>'.$settings->title.'</h3>'; }?>
		<?php if ($settings->subtitle){ echo '<h4>'.$settings->subtitle.'</h4>'; }?>
		<?php echo $settings->content; ?>
	</div>
	<a href="#" class="more"><span>Show More</span> <i class="fal fa-chevron-circle-down"></i></a>
</div>