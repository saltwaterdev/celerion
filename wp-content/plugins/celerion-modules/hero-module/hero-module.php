<?php

class HeroModuleClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Hero Module', 'fl-builder' ),
            'description'     => __( 'Build a hero', 'fl-builder' ),
            'group'           => __( 'Header', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'hero-module/',
            'url'             => CELERION_URL . 'hero-module/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'HeroModuleClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Hero Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Hero Settings', 'fl-builder' ),
                'fields'        => array(
	                'hero_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select if Hero Background is Image or Video', 'fl-builder' ),
					    'default'       => 'image',
					    'options'       => array(
					        'image'      => __( 'Image', 'fl-builder' ),
					        'video'      => __( 'Video', 'fl-builder' )
					    )
					),
                    'hero_bg' => array(
					    'type'          => 'photo',
					    'label'         => __('Hero Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'subtitle' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Subtitle', 'fl-builder' ),
					    'class'         => 'subtitle',
					    'description'   => __( 'If left blank, no subtitle will show', 'fl-builder' ),
					    'help'          => __( 'This is typically the title of the page and will has a colored box to the left.', 'fl-builder' )
					),
					'background_color' => array(
					    'type'          => 'color',
					    'label'         => __( 'Background Color', 'fl-builder' ),
					    'default'       => 'ffffff',
					    'show_reset'    => true,
					    'show_alpha'    => true
					),
					'box_color' => array(
					    'type'          => 'color',
					    'label'         => __( 'Subtitle Box Color', 'fl-builder' ),
					    'show_reset'    => true,
					    'show_alpha'    => true
					),
                    'hero_content' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'content_position' => array(
					    'type'          => 'select',
					    'label'         => __( 'Content Position', 'fl-builder' ),
					    'default'       => 'left',
					    'options'       => array(
					        'left'      => __( 'Left', 'fl-builder' ),
					        'right'      => __( 'Right', 'fl-builder' )
					    )
					),
					'image_alignment_vertical' => array(
					    'type'          => 'select',
					    'label'         => __( 'Image Vertical Alignment', 'fl-builder' ),
					    'default'       => 'top',
					    'options'       => array(
					        'top'      => __( 'Top', 'fl-builder' ),
					        'center'      => __( 'Center', 'fl-builder' ),
					        'bottom'      => __( 'Bottom', 'fl-builder' )
					    )
					),
					'image_alignment_horizontal' => array(
					    'type'          => 'select',
					    'label'         => __( 'Image Horizontal Alignment', 'fl-builder' ),
					    'default'       => 'center',
					    'options'       => array(
					        'center'      => __( 'Center', 'fl-builder' ),
					        'left'      => __( 'Left', 'fl-builder' ),
					        'right'      => __( 'Right', 'fl-builder' )
					    )
					),
                )
            )
        )
    ),
    'my-tab-3'      => array(
        'title'         => __( 'Video Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-3'  => array(
                'title'         => __( 'Video Settings', 'fl-builder' ),
                'fields'        => array(
	                'video_type'       => array(
						'type'          => 'select',
						'label'         => __( 'Video Type', 'fl-builder' ),
						'default'       => 'wordpress',
						'options'       => array(
							'media_library'     => __( 'Media Library', 'fl-builder' ),
							'embed'             => __( 'Embed', 'fl-builder' ),
						),
						'toggle'        => array(
							'media_library'      => array(
								'fields'      => array( 'video', 'video_webm', 'poster', 'autoplay', 'loop' ),
							),
							'embed'     => array(
								'fields'      => array( 'video_url' ),
							),
						),
					),
					'video'          => array(
						'type'          => 'video',
						'label'         => __( 'Video (MP4)', 'fl-builder' ),
						'help'          => __( 'A video in the MP4 format. Most modern browsers support this format.', 'fl-builder' ),
						'show_remove'   => true,
					),
					'video_webm' => array(
						'type'          => 'video',
						'label'         => __( 'Video (WebM)', 'fl-builder' ),
						'help'          => __( 'A video in the WebM format to use as fallback. This format is required to support browsers such as FireFox and Opera.', 'fl-builder' ),
						'preview'         => array(
							'type'            => 'none',
						),
					),
					'loop'           => array(
						'type'          => 'select',
						'label'         => __( 'Loop', 'fl-builder' ),
						'default'       => '1',
						'options'       => array(
							'0'             => __( 'No', 'fl-builder' ),
							'1'             => __( 'Yes', 'fl-builder' ),
						),
						'preview'       => array(
							'type'          => 'none',
						),
					),
					'video_url' => array(
					    'type'          => 'text',
					    'label'         => __( 'Video URL', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => 'video_url',
					    'description'   => __( 'Enter Video URL', 'fl-builder' ),
					    'help'          => __( 'Please make sure video URLs start with https', 'fl-builder' )
					),
                )
            )
        )
    ),
) );