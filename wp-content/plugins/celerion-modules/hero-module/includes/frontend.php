<?php if ($settings->hero_type == "video"){  ?>
	<div class="background-video video-container">
		<video autoplay="" <?php if ($settings->loop == 1){ ?>loop=""<?php } ?>>
			<?php if ($settings->video_type == 'media_library'){ ?>
			<source src="<?php echo wp_get_attachment_url($settings->video); ?>" type="video/mp4">
			<source src="<?php echo wp_get_attachment_url($settings->video_webm); ?>" type="video/webm">
			<?php } else { ?>
			<source src="<?php echo $settings->video_url; ?>" type="video/mp4">
			<?php } ?>
		</video>
	</div>
<?php } ?>
<div class="content <?php echo $settings->content_position; ?>">
	<?php if ($settings->subtitle){ ?>
	<h1><?php if ($settings->box_color){ ?><span></span><?php } ?><?php echo $settings->subtitle; ?></h1>
	<hr>
	<?php } ?>
	<?php echo $settings->hero_content; ?>
</div>
<a href="#" class="slide_down"></a>