.fl-node-<?php echo $id; ?> {
    background-color: #<?php echo $settings->background_color; ?>;
    background-image: url(<?php echo wp_get_attachment_url($settings->hero_bg); ?>);
    width: 100%;
	height: 78vh;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: <?php echo $settings->image_alignment_vertical; ?> <?php echo $settings->image_alignment_horizontal; ?>;
    border-radius: 5px;
    display: flex;
	justify-content: center;
	font-size: 85%;
	position: relative; 
	max-height: 620px;
}

.fl-node-<?php echo $id; ?> .background-video{
		display: block;
	    position: absolute;
	    left: 0;
	    top: 0;
	    width: 100%;
	    height: 500px;
	    overflow: hidden;
	    border-radius: 5px;
	}
	.fl-node-<?php echo $id; ?> .background-video video{
		width: 124%;
		margin: auto;
		  position: absolute;
		  top: 0; left: 0; bottom: 0; right: 0;
	}

.fl-node-<?php echo $id; ?>:before{
	content: "";
    position: absolute;
    top: 0;
    left: 0;
    z-index: 2;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    -moz-transition: background .15s linear;
    -webkit-transition: background .15s linear;
    -o-transition-property: background .15s linear;
    -ms-transition-property: background .15s linear;
    transition-property: background .15s linear;
    background: -moz-linear-gradient(left, rgba(18,23,22,0.8) 0%, rgba(18,23,22,0) 100%);
    background: -webkit-linear-gradient(left, rgba(18,23,22,0.8) 0%,rgba(18,23,22,0) 100%);
    background: linear-gradient(to right, rgba(18,23,22,0.8) 0%,rgba(18,23,22,0) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#121716', endColorstr='#00121716',GradientType=1 );
}

.fl-node-<?php echo $id; ?>.home {
	height: 78vh;
}

.fl-node-<?php echo $id; ?>.home:before{
	display: none;
}

.fl-node-<?php echo $id; ?> .slide_down{
	display: none;
}

.fl-node-<?php echo $id; ?>.home .slide_down{
	display: block;
	position: absolute;
	bottom: 45px;
	left: 0;
	right: 0;
	height: 49px;
	width: 20px;
	background-position: center center;
	background-repeat: no-repeat;
	background-size: contain;
	margin: auto;
	background-image: url(/wp-content/themes/celerion/img/scroll_img.png);
}

.fl-node-<?php echo $id; ?>.home .slide_down:hover{
	-webkit-animation: bump 1s infinite;
	-moz-animation:    bump 1s infinite;
	-o-animation:      bump 1s infinite;
	animation:         bump 1s infinite;
}

.fl-node-<?php echo $id; ?> .fl-module-content{
	margin: 0 4.2%;
	align-self: center;
	width: 100%;
	z-index: 2;
}

.fl-node-<?php echo $id; ?> .content{
	width: 100%;
	max-width: 702px;
	position: relative;
	z-index: 2;
	float: <?php echo $settings->content_position; ?>;
}

.fl-node-<?php echo $id; ?> .content.right{
	max-width: 629px;
}

.fl-node-<?php echo $id; ?> hr{
	border: 0;
    height: 1px;
    background: #fff;
    margin: 1.5em auto 1em;
}

.fl-node-<?php echo $id; ?> h2{
	color: white;
	font-weight: 400;
	font-family: 'Helvetica Neue LT W01_75 Bold';
	margin: 0;
	font-size: 3.8em;
	line-height: 1.1em;
}

.fl-node-<?php echo $id; ?>.home h2 {
	font-size: 4.2em;
}

.fl-node-<?php echo $id; ?> h3{
	color: white;
	font-weight: 400;
	font-family: 'Helvetica Neue LT W01_75 Bold';
	margin: 0;
	font-size: 2.65em;
	line-height: 1.25em;
}

.fl-node-<?php echo $id; ?> p{
	color: white;
	font-size: 1.25em;
	margin: 0.5em 0 1em;
}

.fl-node-<?php echo $id; ?> h1{
	color: white;
	font-weight: 400;
	font-family: 'Helvetica Neue LT W01_75 Bold';
	font-size: 1.17em;
	margin: 0;
}

.fl-node-<?php echo $id; ?> h1 span{
	height: 0.8em;
	width: 0.8em;
	display: inline-block;
	background-color: #<?php echo $settings->box_color; ?>;
	margin-right: 0.5em;
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> {
    	font-size: 100%;
	}
	.fl-node-<?php echo $id; ?>, .fl-node-<?php echo $id; ?>.home {
		height: 500px;
	}
}
@media only screen and (min-width: 1024px) {
	.fl-node-<?php echo $id; ?>, .fl-node-<?php echo $id; ?>.home, .fl-node-<?php echo $id; ?> .background-video{
		height: 620px;
	}
}
@-webkit-keyframes bump {
  0%   { bottom: 40px; }
  50%  { bottom: 50px; }
  100% { bottom: 40px; }
}
@-moz-keyframes bump {
  0%   { bottom: 40px; }
  50%  { bottom: 50px; }
  100% { bottom: 40px; }
}
@-o-keyframes bump {
  0%   { bottom: 40px; }
  50%  { bottom: 50px; }
  100% { bottom: 40px; }
}
@keyframes bump {
  0%   { bottom: 40px; }
  50%  { bottom: 50px; }
  100% { bottom: 40px; }
}