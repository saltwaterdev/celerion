<?php 

$pageid = url_to_postid($settings->parent); 
$currentid = get_the_ID();
if ($settings->num){
	$num = $settings->num;
} else {
	$num = 3;
}

	$my_wp_query = new WP_Query();
	$querySearch = array(
		'post_type' => 'news',
	    'meta_key' => 'conference_start',
	    'meta_compare' => '>=',
	    'meta_value' => date ('Y-m-d'),
	    'meta_type'  => 'DATE',
	    'post_status' => 'publish',
	    'posts_per_page' => $num,
	    'orderby' => 'meta_value',
	    'order' => 'ASC'
	);
	$all_wp_pages = $my_wp_query->query($querySearch);


?>

<div class="page_list <?php if ($settings->template == 2){ echo "horizontal";}; ?>">
   <h4><?php echo $settings->title; ?></h4>
   <?php foreach ($all_wp_pages as $page){ ?>
   		<div class="item">
	   		<?php 
		   	
		   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
		   		$style = "contain";	
		   		if (get_post_type($page->ID) == "resource"){
			   		$terms = get_the_terms($page->ID, 'type');
			   		if ( $terms && ! is_wp_error( $terms ) ){
						foreach ( $terms as $term ) {
							if ($term->slug == "articles"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/article.png); background-size: 55%;"';
							} else if ($term->slug == "case-study"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/case_study.png); background-size: 55%;"';
							} else if ($term->slug == "presentation"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/presentation.png); background-size: 55%;"';
							} else if ($term->slug == "recorded-webex"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
							} else if ($term->slug == "scientific-poster"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/scientific_poster.png); background-size: 55%;"';
							} else if ($term->slug == "video"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/video.png); background-size: 55%;"';
		    				} else if ($term->slug == "webinar"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
		    				} else if ($term->slug == "white-papers"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/whitepaper.png); background-size: 55%;"';
		    				} else if ($term->slug == "fact-sheet"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/fact-sheet-icon.png); background-size: 55%;"';
		    				} else{
			    				$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
		    				}
						}
					} else {
						$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
					}
			   	} else {
				   	if ($thumb == 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"'){
					   	if (get_post_type($page->ID) == "event"){
						   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/event.png); background-size: 55%;"';
					   	} else {
						   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/news.png); background-size: 50%;"';
					   	}
				   	}
			   	}	
			   	if ($pageid == 4656){
				   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/blog.png); background-size: 50%;"';
			   	}
		   		
		   	?>
        	<a href="<?php echo get_permalink($page->ID); ?>" class="img" <?php echo $thumb; ?>></a>
			<div class="content">
				<?php
					$target = "";
					if (get_post_type($page->ID) == "resource"){
						$pagelink = get_permalink($page->ID);
						$resourceinfo = get_post_meta($page->ID);
						if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
							$pagelink = $resourceinfo['resource_file'][0];
							$target = 'target="_blank"';
						}
						if ($resourceinfo['external_link'][0] && $resourceinfo['external_link'][0] !== ""){
							$pagelink = $resourceinfo['external_link'][0];
							$target = 'target="_blank"';
						}
					} else {
						$pagelink = get_permalink($page->ID);
						$resourceinfo = get_post_meta($page->ID);
						if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
							$pagelink = $resourceinfo['resource_file'][0];
							$target = 'target="_blank"';
						}
						if ($resourceinfo['external_link'][0] && $resourceinfo['external_link'][0] !== ""){
							$pagelink = $resourceinfo['external_link'][0];
							$target = 'target="_blank"';
						}
					}
				?>
            	<a href="<?php echo $pagelink; ?>" class="title" <?php echo $target; ?>><?php echo get_the_title($page->ID); ?></a>
            	<?php if (get_post_type($page->ID) == "event"){ $eventinfo = get_post_meta( $page->ID ); ?>
	            	<?php 
				        if ( isset ( $eventinfo['conference_start'] ) ){
				        	$startdatearray = explode("-", $eventinfo['conference_start'][0]);
				        }
				        if ( isset ( $eventinfo['conference_end'] ) ){
				        	$enddatearray = explode("-", $eventinfo['conference_end'][0]);
				        }
			        ?>
            		<p class="by"><span class="date"><?php if ( isset ( $eventinfo['conference_start'] ) ) echo $startdatearray[1].'.'.$startdatearray[2].'.'.$startdatearray[0]; ?> <?php if ( isset ( $eventinfo['conference_end'] ) ) echo ' - '. $enddatearray[1].'.'.$enddatearray[2].'.'.$enddatearray[0]; ?></span> <!-- - <span class="name">By John Smith</span> --></p>
            	<?php } else if (get_post_type($page->ID) == "resource"){ ?>
	            	<?php
						$terms = get_the_terms($page->ID, 'type');
						$types = "";
						$termscount = count($terms);
						$termcount = 1;
						if ( $terms && ! is_wp_error( $terms ) ){
							foreach ( $terms as $term ) {
								$types .= $term->name;
								if ($termcount !== $termscount){
									$types .= " | ";
								}
								$termcount++;
							}
						}	
					?>
            		<p class="by"><span class="date"><?php echo $types; ?></span> <!-- - <span class="name">By John Smith</span> --></p>
				<?php } else { ?>
					<p class="by"><span class="date"><?php echo get_the_date( 'm.d.y', $page->ID ); ?></span> <!-- - <span class="name">By John Smith</span> --></p>
				<?php } ?>
        	</div>
    	</div>
   <?php } ?>
   <?php if ($settings->include_link == 1){ ?>
   	 	<a href="<?php echo $settings->parent; ?>" class="more">All <?php echo $settings->title; ?> <i class="fal fa-chevron-circle-right"></i></a>
    <?php } ?>
</div>