<?php

class NewsListClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'News List', 'fl-builder' ),
            'description'     => __( 'Build a list of news', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'news-list/',
            'url'             => CELERION_URL . 'news-list/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'NewsListClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'News List Settings', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'Enter Title for News List block', 'fl-builder' ),
					    'help'          => __( 'If left blank, will not show.', 'fl-builder' )
					),
                    'parent' => array(
					    'type'          => 'link',
					    'label'         => __('Show Pages Under:', 'fl-builder')
					),
					'num' => array(
					    'type'          => 'text',
					    'label'         => __( 'Number of Pages', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Number of Pages', 'fl-builder' ),
					    'class'         => 'num',
					    'description'   => __( 'Enter number of pages to display.', 'fl-builder' ),
					    'help'          => __( 'If left blank, will default to three.', 'fl-builder' )
					),
					'tag' => array(
					    'type'          => 'text',
					    'label'         => __( 'Tag', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Tag', 'fl-builder' ),
					    'class'         => 'tag',
					    'description'   => __( 'If you want a list of pages with specific tags, enter tag here.', 'fl-builder' ),
					    'help'          => __( 'Optional', 'fl-builder' )
					),
					'include_link' => array(
					    'type'          => 'select',
					    'label'         => __( 'Include Link to Parent Page?', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Yes', 'fl-builder' ),
					        '2'      => __( 'No', 'fl-builder' )
					    ),
					),
					'template' => array(
					    'type'          => 'select',
					    'label'         => __( 'Vertical or Horizontal Layout?', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Vertical', 'fl-builder' ),
					        '2'      => __( 'Horizontal', 'fl-builder' )
					    ),
					),
                )
            )
        )
    )
) );