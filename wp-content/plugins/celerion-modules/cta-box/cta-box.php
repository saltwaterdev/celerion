<?php

class CtaBoxClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'CTA Box', 'fl-builder' ),
            'description'     => __( 'Create a CTA Box', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'cta-box/',
            'url'             => CELERION_URL . 'cta-box/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'CtaBoxClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'CTA Box Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
	                'key_color' => array(
					    'type'          => 'color',
					    'label'         => __( 'Key Color', 'fl-builder' ),
					    'default'       => 'A8085C',
					    'show_reset'    => true,
					    'show_alpha'    => true
					),
                    'bg' => array(
					    'type'          => 'photo',
					    'label'         => __('Background Image', 'fl-builder'),
					    'show_remove'   => false,
					),
                    'title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Box Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '400',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'If left blank, will default to CTA page link title', 'fl-builder' ),
					    'help'          => __( 'This will be the largest text in the box', 'fl-builder' )
					),
					'subtitle' => array(
					    'type'          => 'text',
					    'label'         => __( 'Optional Subtitle', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '400',
					    'size'          => '40',
					    'placeholder'   => __( 'Subtitle', 'fl-builder' ),
					    'class'         => 'subtitle',
					    'description'   => __( 'This field is optional.  If blank, will not display', 'fl-builder' ),
					    'help'          => __( 'Appears smaller, above the title', 'fl-builder' )
					),
					'cta_link' => array(
					    'type'          => 'link',
					    'label'         => __('CTA Link', 'fl-builder')
					),
					'cta' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '400',
					    'size'          => '40',
					    'placeholder'   => __( 'CTA Text', 'fl-builder' ),
					    'class'         => 'cta',
					    'description'   => __( 'If left blank, will default to Learn More', 'fl-builder' ),
					    'help'          => __( 'This is what the clickable link text will display', 'fl-builder' )
					),
                )
            )
        )
    )
) );