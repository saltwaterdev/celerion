.fl-node-<?php echo $id; ?> {
	background-color: #121716;
    width: 100%;
    height: 30em;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    border-radius: 5px;
    margin-bottom: 1.9em;
    overflow: hidden;
    position: relative;
    font-size: 90%;
}
.page_home .fl-node-<?php echo $id; ?> {
	height: 38.7em;
}
.fl-node-<?php echo $id; ?> .background{
	background-image: url(<?php echo wp_get_attachment_url($settings->bg); ?>);
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	left: -12%;
    width: 124%;
    top: -4.4em;
    opacity: 1;
    height: calc(100% + 8.8em);
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
	-moz-transition: transform .15s linear;
	-webkit-transition: transform .15s linear;
	-o-transition-property: transform .15s linear;
	-ms-transition-property: transform .15s linear;
	transition-property: transform .15s linear;
}

.fl-node-<?php echo $id; ?> .background:before{
	content: "";
	position: absolute;
	top: 0;
	left: 0;
	z-index: 2;
	width: 100%;
	height: 100%;
	-moz-transition: background .15s linear;
	-webkit-transition: background .15s linear;
	-o-transition-property: background .15s linear;
	-ms-transition-property: background .15s linear;
	transition-property: background .15s linear;
	background: -moz-linear-gradient(left, rgba(18,23,22,0.8) 0%, rgba(18,23,22,0) 100%);
	background: -webkit-linear-gradient(left, rgba(18,23,22,0.8) 0%,rgba(18,23,22,0) 100%);
	background: linear-gradient(to right, rgba(18,23,22,0.8) 0%,rgba(18,23,22,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#121716', endColorstr='#00121716',GradientType=1 );
}

.fl-node-<?php echo $id; ?>:hover .background:before{
	background: rgba(18,23,22,0.5);
}

.fl-node-<?php echo $id; ?>:hover .background{
	transform: scale(1.05);
}

.fl-node-<?php echo $id; ?> .content{
	height: 100%;
}

.fl-node-<?php echo $id; ?> .fl-module-content{
	height: 100%;
    padding: 3.85em 0em;
    width: 87%;
    margin: 0 auto;
    max-width: 604px;	
}

.fl-node-<?php echo $id; ?> hr{
	border: 0;
	height: 1px;
	background: #fff;
	width: 100%;
	position: relative;
	z-index:3;
	margin: 1.0em auto;
}

.fl-node-<?php echo $id; ?> p.subtitle{
	color: white;
	font-family:'Helvetica Neue LT W01_75 Bold';
	font-size: 1.1em;
	margin: 0;
}

.fl-node-<?php echo $id; ?> p{
	position: relative;
	z-index: 3;
}

.fl-node-<?php echo $id; ?> p.title{
	color: white;
	font-family:'Helvetica Neue LT W01_75 Bold';
	font-size: 3.1em;
	line-height: 1.1em;
	margin: 0;
}

.fl-node-<?php echo $id; ?> a{
	color: white;
    font-family: 'Helvetica Neue LT W01_75 Bold';
    font-size: 1.2em;
    position: absolute;
    left: 0;
    bottom: 3.2em;
    z-index: 5;
    width: 87%;
    max-width: 604px;
    right: 0;
    margin: auto;
    -moz-transition: left .15s linear;
	-webkit-transition: left .15s linear;
	-o-transition-property: left .15s linear;
	-ms-transition-property: left .15s linear;
	transition-property: left .15s linear;
}

.fl-node-<?php echo $id; ?> a.cover{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 4;
	max-width: 100%;
	bottom: 0;
	opacity: 0.0;
	-moz-transition: opacity .15s linear;
	-webkit-transition: opacity .15s linear;
	-o-transition-property: opacity .15s linear;
	-ms-transition-property: opacity .15s linear;
	transition-property: opacity .15s linear;
}

.fl-node-<?php echo $id; ?> a.cover:hover{
	opacity: 0.7;
}

.fl-node-<?php echo $id; ?> a i{
	color: #<?php echo $settings->key_color; ?>;
	font-size: 1.2em;
    position: relative;
    top: 0.05em;
    margin-left: 0.25em;
    left: 0;
    -moz-transition: color .15s linear;
	-webkit-transition: color .15s linear;
	-o-transition-property: color .15s linear;
	-ms-transition-property: color .15s linear;
	transition-property: color .15s linear;
	font-weight: bold;
}

.fl-node-<?php echo $id; ?>:hover a i{
	color: white;	
}

.fl-node-<?php echo $id; ?> p.subtitle span{
	background-color: #<?php echo $settings->key_color; ?>;
	width: 0.844em;
    height: 0.844em;
    display: inline-block;
    margin-right: 0.8em;
    -moz-transition: background-color .15s linear;
	-webkit-transition: background-color .15s linear;
	-o-transition-property: background-color .15s linear;
	-ms-transition-property: background-color .15s linear;
	transition-property: background-color .15s linear;
}
.fl-node-<?php echo $id; ?> .shield {
	position: absolute;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
    background-color: #<?php echo $settings->key_color; ?>;
    top: 0;
    z-index: 2;
    opacity: 0.0;
    -moz-transition: opacity .15s linear;
	-webkit-transition: opacity .15s linear;
	-o-transition-property: opacity .15s linear;
	-ms-transition-property: opacity .15s linear;
	transition-property: opacity .15s linear;
}
.fl-node-<?php echo $id; ?>:hover .shield{
	opacity: 0.6;
}
.fl-node-<?php echo $id; ?>:hover p.subtitle span{
	background-color: white;
}
@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> {
		font-size: 95%;	
	}
}

@media only screen and (min-width: 1200px) {
	.fl-node-<?php echo $id; ?> {
		font-size: 100%;	
	}
}