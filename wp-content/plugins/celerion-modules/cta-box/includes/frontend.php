<?php if (empty($settings->cta)){
	$ctatitle = "Learn More";
} else {
	$ctatitle = $settings->cta;
}
$thispage = get_the_ID();
?>

<div class="content<?php if (wp_get_post_parent_id( $thispage ) == 3062){echo " innovation"; } ?>">
	<div class="background"></div>
	<div class="shield"></div>
	<a href="<?php echo $settings->cta_link; ?>" class="cover"></a>
	<?php if ($settings->subtitle){ ?>
		<p class="subtitle"><span class="block"></span> <?php echo $settings->subtitle; ?></p>
		<hr>
	<?php } ?>
	<p class="title"><?php echo $settings->title; ?></p>
	<a href="<?php echo $settings->cta_link; ?>"><?php echo $ctatitle; ?> <i class="fal fa-chevron-circle-right"></i></a>
</div>