<?php

class ImageAndCtaClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Image and Content + CTA', 'fl-builder' ),
            'description'     => __( 'Build a block with an image on one side and content with a CTA on another', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'image-and-cta/',
            'url'             => CELERION_URL . 'image-and-cta/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'ImageAndCtaClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Image and Content + CTA', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'image' => array(
					    'type'          => 'photo',
					    'label'         => __('Image', 'fl-builder'),
					    'show_remove'   => false,
					),
					'imageside' => array(
					    'type'          => 'select',
					    'label'         => __( 'Which Side Should the Image Be On?', 'fl-builder' ),
					    'default'       => 'left',
					    'options'       => array(
					        'left'      => __( 'Left', 'fl-builder' ),
					        'right'      => __( 'Right', 'fl-builder' )
					    )
					),
					'color' => array(
					    'type'          => 'color',
					    'label'         => __( 'Photo and Arrow Background Color', 'fl-builder' ),
					    'default'       => 'b1005d',
					    'show_reset'    => true,
					    'show_alpha'    => true
					),
                    'content' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'link' => array(
					    'type'          => 'link',
					    'label'         => __('Optional Hyperlink Overlay', 'fl-builder')
					)
                )
            )
        )
    )
) );