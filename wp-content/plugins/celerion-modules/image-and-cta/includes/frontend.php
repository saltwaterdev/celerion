<div class="content">
	<a href="<?php echo $settings->link; ?>" class="toplink"></a>
	<?php if ($settings->imageside == "left" || !$settings->imageside){ ?>
		<div class="image image_<?php echo $settings->imageside; ?>"></div>
	<?php } ?>
	<div class="text">
		<div class="content_container">
			<?php echo $settings->content; ?>
		</div>
	</div>
	<?php if ($settings->imageside == "right"){ ?>
		<div class="image"></div>
	<?php } ?>
</div>