.fl-node-<?php echo $id; ?> {
    background-color: #ffffff;
    width: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    border-radius: 5px;
    border: 1px solid #232827;
    margin-bottom: 1.9em;
    position: relative;
}

.fl-node-<?php echo $id; ?> .image {
    background-color: #<?php echo $settings->color; ?>;
    background-image: url(<?php echo wp_get_attachment_url($settings->image); ?>);
    width: calc(100% + 2px);
    height: 0;
    padding-bottom: 52%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    float: <?php echo $settings->imageside; ?>;
    position: relative;
    order: 1;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    top: -1px;
    left: -1px;
}

.fl-node-<?php echo $id; ?> .image:after{
	content: "";
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	opacity: 0;
	background-color: #<?php echo $settings->color; ?>;
	-moz-transition: opacity 0.15s linear;
	-webkit-transition: opacity 0.15s linear;
	-o-transition-property: opacity 0.15s linear;
	-ms-transition-property: opacity 0.15s linear;
	transition-property: opacity 0.15s linear;
	 border-radius: 5px;
}

.fl-node-<?php echo $id; ?> .content:hover .image:after{

}

.fl-node-<?php echo $id; ?> a.toplink{
	opacity: 0.0;
	position: absolute;
	left: 0;
	border-radius: 5px;
	top: 0;
	width: 100%;
	height: 100%;
	z-index: 3;
}

.fl-node-<?php echo $id; ?> .text{
	align-self: center;
	width: 100%;
	float: left;
	align-self: center;
	order: 2;
}

.fl-node-<?php echo $id; ?> .text .content_container{
	width: 93%;
	max-width: 525px;
	margin: 0 auto;
	padding: 3em 0;
}

.fl-node-<?php echo $id; ?> .content{
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	position: relative;
	border-radius: 5px;
	background-color: transparent;
	-moz-transition: background-color 0.15s linear;
	-webkit-transition: background-color 0.15s linear;
	-o-transition-property: background-color 0.15s linear;
	-ms-transition-property: background-color 0.15s linear;
	transition-property: background-color 0.15s linear;
}

.fl-node-<?php echo $id; ?> .content:hover{
	background-color: #<?php echo $settings->color; ?>;
}

.fl-node-<?php echo $id; ?> hr{
	border: 0;
    height: 1px;
    background: #232827;
    margin: 0.5em auto;
    -moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> h1{
	color: white;
	font-weight: 400;
	font-family: 'Helvetica Neue LT W01_75 Bold';
	margin: 0;
	font-size: 4.2em;
	-moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> h2{
	color: #232827;
	font-weight: 400;
	font-family: 'Helvetica Neue LT W01_75 Bold';
	font-size: 3em;
	margin: 0;
	margin-bottom: 0.3em;
	padding-top: 0.25em;
	-moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> h3{
	color: #232827;
	font-weight: 400;
	font-family: 'Helvetica Neue LT W01_75 Bold';
	font-size: 2.65em;
	margin: 0;
	margin-bottom: 0.3em;
	padding-top: 0.25em;
	-moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> p{
	margin: 0;
	line-height: 1.7em;
	margin-bottom: 1.2em;
	-moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> p:last-child{
	margin-bottom: 0;
}

.fl-node-<?php echo $id; ?> a{
	font-family: 'Helvetica Neue LT W01_75 Bold';
	position: relative;
	-moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> a .arrow:after{
    color: #<?php echo $settings->color; ?>;
    content: "\f138";
    display: inline-block;
    font-family: "Font Awesome 5 Pro";
    font-size: 1.17em;
    margin-left: 0.5em;
    position: relative;
    top: 2px;
    -moz-transition: color 0.15s linear;
	-webkit-transition: color 0.15s linear;
	-o-transition-property: color 0.15s linear;
	-ms-transition-property: color 0.15s linear;
	transition-property: color 0.15s linear;
}

.fl-node-<?php echo $id; ?> .content:hover a .arrow:after, .fl-node-<?php echo $id; ?> .content:hover a, .fl-node-<?php echo $id; ?> .content:hover p, .fl-node-<?php echo $id; ?> .content:hover h1, .fl-node-<?php echo $id; ?> .content:hover h2, .fl-node-<?php echo $id; ?> .content:hover h3, .fl-node-<?php echo $id; ?> .content:hover h4, .fl-node-<?php echo $id; ?> .content:hover h5{
	color: white;
}

.fl-node-<?php echo $id; ?> .content:hover h1, .fl-node-<?php echo $id; ?> .content:hover h2, .fl-node-<?php echo $id; ?> .content:hover h3, .fl-node-<?php echo $id; ?> .content:hover h4, .fl-node-<?php echo $id; ?> .content:hover h5{
	
}

.fl-node-<?php echo $id; ?> .content:hover hr{
	background: white;
}

.wp-editor .fl-node-<?php echo $id; ?> a.toplink{
	display: none;	
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> .text .content_container{
		padding: 0;	
	}
	.fl-node-<?php echo $id; ?> {
		height: 38.813em;	
	}
	.fl-node-<?php echo $id; ?> .image {
		width: 50%;	
		height: 38.813em;
		order: unset;
		border-bottom-left-radius: 0px;
	    border-top-right-radius: 5px;
	    border-bottom-right-radius: 5px;
	    border-top-left-radius: 0px;
	    top: -1px;
	    right: -1px;
	    left: 0;
	    padding-bottom: 0;
	    order: 2
	}
	.fl-node-<?php echo $id; ?> .text{
		width: 50%;	
		order: unset;
		order: 1;
	}
	.fl-node-<?php echo $id; ?> .content{
		flex-direction: row;	
	}
	.fl-node-<?php echo $id; ?> .image_left{
		top: -1px;
	    left: -1px;
	    right: 0;
	    border-bottom-left-radius: 5px;
	    border-top-right-radius: 0;
	    border-bottom-right-radius: 0px;
	    border-top-left-radius: 5px;
	    order: 1;
	}
	
	.fl-node-<?php echo $id; ?> .image_right{
		top: -1px;
	    right: -1px;
	    border-bottom-left-radius: 0px;
	    border-top-right-radius: 5px;
	    border-bottom-right-radius: 5px;
	    border-top-left-radius: 0px;
	    order: 2;
	}
}