<?php

class AutonavClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Autonav', 'fl-builder' ),
            'description'     => __( 'Build an autonav', 'fl-builder' ),
            'group'           => __( 'Header', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'autonav/',
            'url'             => CELERION_URL . 'autonav/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'AutonavClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Autonav Settings', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'parent' => array(
					    'type'          => 'link',
					    'label'         => __('Display Pages Under:', 'fl-builder')
					),
					'include_parent' => array(
					    'type'          => 'select',
					    'label'         => __( 'Include Parent Page?', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Include', 'fl-builder' ),
					        '2'      => __( 'Do Not Include', 'fl-builder' )
					    )
					),
                )
            )
        )
    )
) );