.fl-node-<?php echo $id; ?> ul {
    list-style: none;
	margin: 0;
	padding: 0;
}

.fl-node-<?php echo $id; ?> li{
	border-bottom: 1px solid #232827;
	margin-bottom: 0 !important;
}

.fl-node-<?php echo $id; ?> li:before{
	display: none;
	content: "";
}

.fl-node-<?php echo $id; ?> li a{
	font-family:'Helvetica Neue LT W01_75 Bold';
	color: #232827;
	font-size: 1.1rem;
	padding: 1.13em 0;
	display: block;	
	-moz-transition: color 0.15s linear;
	-webkit-transition: color 0.15s linear;
	-o-transition-property: color 0.15s linear;
	-ms-transition-property: color 0.15s linear;
	transition-property: color 0.15s linear;
}

.fl-node-<?php echo $id; ?> li:first-child a{
	padding-top: 0.13em;
}

.fl-node-<?php echo $id; ?> li a.current{
	color: #727f79;
}