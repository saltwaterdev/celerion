<ul class="side_nav">

<?php 
	$pageid = url_to_postid($settings->parent); 
	$currentid = get_the_ID();
	$current = "";
	if ($settings->include_parent == 1){
		if ($currentid == $pageid){
			$current = 'class="current"';
		}
		if (get_the_title($pageid) == "Contact"){
			$pagetitle = "Contact";
		} else {
			$pagetitle = "Overview";
		}
		echo '<li><a href="'.$settings->parent.'" '.$current.'>'.$pagetitle.'</a></li>';
	}
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1', 'orderby' => 'menu_order'));
	$childpages = get_page_children( $pageid, $all_wp_pages );

	$packages = get_pages('child_of=' . $pageid .'&sort_column=menu_order&hierarchical=0&parent=' . $pageid); 

	foreach ($packages as $childpage){
		$current = "";
		if ($currentid == $childpage->ID){
			$current = 'class="current"';
		}
		$redirectinfo = get_post_meta( $childpage->ID );
		if (isset($redirectinfo['redirect'][0]) && $redirectinfo['redirect'][0] !== 0){
			$pagelink = get_permalink($redirectinfo['redirect'][0]);
		} else {
			$pagelink = get_permalink($childpage->ID);
		}
		if ($childpage->ID == 4761){
			$pagelink = "/expertise/tobacco-risk-evaluation";
		}
		if ($childpage->ID == 2996){
			$pagelink = "/services/early-phase-services/centers";
		}
		if ($childpage->ID == 2994){
			$pagelink = "/services/early-phase-services";
		}
		if ($childpage->ID == 3002){
			$pagelink = "/services/phase-2-3";
		}
		if ($childpage->ID == 3016){
			$pagelink = "/services/bioanalytical-sciences";
		}
		if ($childpage->ID == 2951){
			$pagelink = "/services/drug-development-regulatory-affairs";
		}
		if ($childpage->ID == 57){
			$pagelink = "/services/data-management-biometrics";
		}
		if ($childpage->ID == 3034){
			$pagelink = "/services/data-management-biometrics/pharmacokinetics-pharmacodynamics-pk-pd";
		}
		echo '<li><a href="'.$pagelink.'" '.$current.'>'.get_the_title($childpage->ID).'</a></li>';
	}
?>

</ul>