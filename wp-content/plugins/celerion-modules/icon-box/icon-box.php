<?php

class IconBoxClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Icon Box', 'fl-builder' ),
            'description'     => __( 'Build a block with an icon and copy', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'icon-box/',
            'url'             => CELERION_URL . 'icon-box/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'IconBoxClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Icon Box', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'icon' => array(
					    'type'          => 'photo',
					    'label'         => __('Icon', 'fl-builder'),
					    'show_remove'   => false,
					),
					'iconside' => array(
					    'type'          => 'select',
					    'label'         => __( 'Which side should the icon be on?', 'fl-builder' ),
					    'default'       => 'left',
					    'options'       => array(
					        'left'      => __( 'Left', 'fl-builder' ),
					        'top'      => __( 'Top', 'fl-builder' ),
					        'right'      => __( 'Right', 'fl-builder' )
					    )
					),
					'title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Icon Box Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '400',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'If left blank, will not show', 'fl-builder' ),
					    'help'          => __( 'This will be the largest text in the box', 'fl-builder' )
					),
					'copy' => array(
					    'type'          => 'textarea',
					    'label'         => __( 'Icon Box Copy', 'fl-builder' ),
					    'default'       => '',
					    'placeholder'   => __( 'Icon Box Copy', 'fl-builder' ),
					    'rows'          => '6'
					),
                )
            )
        )
    )
) );