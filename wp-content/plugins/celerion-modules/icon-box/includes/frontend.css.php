.fl-node-<?php echo $id; ?> {
	margin-bottom: 3em;
}
.fl-node-<?php echo $id; ?> .content{
	overflow: hidden;
}
.fl-node-<?php echo $id; ?> .content.left, .fl-node-<?php echo $id; ?> .content.right{
	border-top: 1px solid #000;
    padding-top: 3em;
}
.fl-node-<?php echo $id; ?> .icon{
	width: 155px;
	height: 155px;
	border-radius: 50%;
	background-image: url(<?php echo wp_get_attachment_url($settings->icon); ?>);
	background-size: 100px;
	background-repeat: no-repeat;
	background-position: center center;
	margin: 0 auto 3em;
}
.fl-node-<?php echo $id; ?> .icon.empty{
	background-color: #d3d2d3;
	border: 1px solid #979797;
}
.fl-node-<?php echo $id; ?> .copy{
    width: 100%;
	text-align: center;
}
.fl-node-<?php echo $id; ?> .copy h4{
	
}
.fl-node-<?php echo $id; ?> .copy p{
	margin-bottom: 0;
}
@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> .icon{
		float: left;
		margin: 0;
	}
	.fl-node-<?php echo $id; ?> .copy{
		float: left;
	    width: calc(92% - 155px);
	    margin-left: 8%;
	    text-align: left;
	}
	.fl-node-<?php echo $id; ?> .content.left, .fl-node-<?php echo $id; ?> .content.right{
		display: flex;
	}
	.fl-node-<?php echo $id; ?> .content.left .copy, .fl-node-<?php echo $id; ?> .content.right .copy{
		align-self: center;
	}
	.fl-node-<?php echo $id; ?> .content.top .icon{
		float: none;
		margin: 0 auto;
	}
	.fl-node-<?php echo $id; ?> .content.top .copy{
		margin-left: 0;
	    width: 100%;
	    text-align: center;
	}
	.fl-node-<?php echo $id; ?> .content.right .icon{
		float: right;
	}
	.fl-node-<?php echo $id; ?> .content.right .copy{
		margin-left: 0;
	}
	.fl-node-<?php echo $id; ?> .content.top .icon{
		float: none;
		margin: 0 auto;
	}
	.fl-node-<?php echo $id; ?> .content.top .icon{
		margin: 0 auto 3em;
	}
	.fl-node-<?php echo $id; ?> .content.top .copy{
		margin-left: 0;
	    width: 100%;
	    text-align: center;
	}
	.fl-node-<?php echo $id; ?> .content.right .icon{
		float: right;
	}
	.fl-node-<?php echo $id; ?> .content.right .copy{
		margin-left: 0;
	}
}