<?php if (empty($settings->cta)){
	$ctatitle = "Learn More";
} else {
	$ctatitle = $settings->cta;
}
?>

<div class="content iconbox <?php echo $settings->iconside; ?>">
	<div class="icon <?php if (!$settings->icon){echo 'empty';};?>"></div>
	<div class="copy">
		<h4><?php echo $settings->title; ?></h4>
		<p><?php echo $settings->copy; ?></p>
	</div>
</div>