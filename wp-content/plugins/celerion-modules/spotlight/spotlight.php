<?php

class SpotlightClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Spotlight', 'fl-builder' ),
            'description'     => __( 'Build a spotlight module', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => CELERION_DIR . 'spotlight/',
            'url'             => CELERION_URL . 'spotlight/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'SpotlightClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Spotlight Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Spotlight Settings', 'fl-builder' ),
                'fields'        => array(
                    'spotlight_img' => array(
					    'type'          => 'photo',
					    'label'         => __('Spotlight Image', 'fl-builder'),
					    'show_remove'   => true,
					),
					'image_position' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select which side the image should be positioned', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Left', 'fl-builder' ),
					        '2'      => __( 'Right', 'fl-builder' )
					    )
					),
					'spotlight_title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Spotlight Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( 'This appears above the event description box. Typically the largest text', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'spotlight_description' => array(
					    'type'          => 'text',
					    'label'         => __( 'Spotlight Description', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => 'description',
					    'description'   => __( 'This appears above the event description box directly below the spotlight title.', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'event_title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Event Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => 'event_title',
					    'description'   => __( 'This is the title of the event, segmented inside a white box.', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'event_start_date' => array(
					    'type'          => 'text',
					    'label'         => __( 'Event Start Date', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Please format MM.DD.YYYY', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'event_end_date' => array(
					    'type'          => 'text',
					    'label'         => __( 'Event End Date (Optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Please format MM.DD.YYYY', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'event_location' => array(
					    'type'          => 'text',
					    'label'         => __( 'Event Location', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'event_link_title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Event Link Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'The text of the hyperlink to visit the event website', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
					'event_link_url' => array(
					    'type'          => 'text',
					    'label'         => __( 'Event Link URL', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( 'Please use a maximum of 255 characters', 'fl-builder' )
					),
                )
            )
        )
    ),
) );