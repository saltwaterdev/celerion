.fl-node-<?php echo $id; ?> .spotlight{
    background-color: #b1005d;
    max-width: 1537px;
    border-radius: 4px;
    margin: 0m 0 2em;
}

.fl-node-<?php echo $id; ?> .spotlight .container{
	width: 96%;
	padding: 2em 0;
	display: flex;
	flex-direction: column;
}

.fl-node-<?php echo $id; ?> .spotlight .container .img{
	overflow: hidden;
	background-image: url(<?php echo wp_get_attachment_url($settings->spotlight_img); ?>);
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    border-radius: 4px;
    margin-bottom: 1em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .img img{
	border-radius: 4px;
	width: 100%;
	max-width: 100%;
	height: auto;
	opacity: 0;
}

.fl-node-<?php echo $id; ?> .spotlight .container .content{
	
}

.fl-node-<?php echo $id; ?> .spotlight .container .content h3{
	color: white;
	font-size: 2em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .content p{
	color: white;
	font-size: 1.25em;
	margin: 0;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container{
	display: block;
	width: 94%;
	position: relative;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box{
	background-color: #fafafa;
	border-radius: 4px;
	margin-top: 1.5em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container h4{
	font-size: 1.435em;
	margin-bottom: 0.3em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container p{
	font-size: 1.14em;
	color: #232827;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container p.location{
	font-family: "Helvetica Neue LT W01_65 Md";
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container a.link{
	font-family: 'Helvetica Neue LT W01_75 Bold';
	font-size: 1.3em;
	
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container a.link i{
	font-weight: bold;
	margin-left: 6px;
	display: inline-block;
	margin-top: 1em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container a.link:hover{
	text-decoration: underline;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container .sharethis-inline-share-buttons{
	position: absolute;
    right: 0;
    bottom: 2em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container .st-btn{
	background-color: transparent !important;
    min-height: 0 !important;
    height: initial !important;
    line-height: initial !important;
    padding: 0 !important;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container .st-btn img{
	display: none;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container .st-btn:hover{
	top: 0 !important;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container .st-btn span{
	color: #232827 !important;
    display: inline-block !important;
    font-weight: 400 !important;
    min-width: 0 !important;
    padding: 0 !important;
    font-size: 0.94em;
    line-height: initial !important;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container a.share{
	position: absolute;
    right: 0;
    bottom: 2em;
}

.fl-node-<?php echo $id; ?> .spotlight .container .event_box .container a.share:hover{
	text-decoration: underline;
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> .spotlight .container{
		flex-direction: row;	
	}
	.fl-node-<?php echo $id; ?> .spotlight .container .img{
		width: 32.5%;
		margin-right: 2.5%;
		margin-bottom: 0;
	}
	.fl-node-<?php echo $id; ?> .spotlight .container .content{
		width: 65%;
	}
}