<div class="spotlight">
	<div class="container">
		<div class="img">
			<img src="<?php echo wp_get_attachment_url($settings->spotlight_img); ?>">
		</div>
		<div class="content">
			<?php if ($settings->spotlight_title){?><h3><?php echo $settings->spotlight_title; ?></h3><?php } ?>
			<?php if ($settings->spotlight_description){?><p><?php echo $settings->spotlight_description; ?></p><?php } ?>
			<div class="event_box">
				<div class="container">
					<?php if ($settings->event_title){?><h4><?php echo $settings->event_title; ?></h4><?php } ?>
					<?php if ($settings->event_start_date){?>
						<p><?php echo $settings->event_start_date; ?> <?php if ($settings->event_end_date){ ?>- <?php echo $settings->event_end_date; }?></p>
					<?php } ?>
					<?php if ($settings->event_location){?><p class="location"><?php echo $settings->event_location; ?></p><?php } ?>
					<?php if ($settings->event_link_url){?><a href="<?php echo $settings->event_link_url; ?>" target="_blank" class="link"><?php echo $settings->event_link_title; ?> <i class="fal fa-chevron-circle-right"></i></a><?php } ?>
						<div class="sharethis-inline-share-buttons"></div>
<!-- 					<a href="#" class="share">Share</a> -->
				</div>
			</div>
		</div>
	</div>
</div>