.fl-node-<?php echo $id; ?> .page_list, .fl-node-<?php echo $id; ?> .resource_holder, .fl-node-<?php echo $id; ?> .news_holder{
	overflow: hidden;
	margin: 5em 0;
}
.fl-node-<?php echo $id; ?> .page_list h4, .fl-node-<?php echo $id; ?> .resource_holder h4, .fl-node-<?php echo $id; ?> .news_holder h4{
	font-weight: 400;
	font-family:'Helvetica Neue LT W01_75 Bold';
	font-size: 1.25em;
	border-bottom: 1px solid #232827;
	padding: 0 0 1em 0;
	margin: 0;
}
.fl-node-<?php echo $id; ?> .item, .fl-node-<?php echo $id; ?> .item, .fl-node-<?php echo $id; ?> .item{
	border-bottom: 1px solid #232827;
	overflow: hidden;
	padding: 1.625em 0px;
}
.fl-node-<?php echo $id; ?> .item .img, .fl-node-<?php echo $id; ?> .item .img, .fl-node-<?php echo $id; ?> .item .img{
	width: 183px;
	height: 123px;
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
	background-color: #f4f4f4;
	border-radius: 5px;
	float: left;
	background-size: contain;
	position: relative;
	display: inline-block;
}
.fl-node-<?php echo $id; ?> .item .img:after, .fl-node-<?php echo $id; ?> .item .img:after, .fl-node-<?php echo $id; ?> .item .img:after{
	position: absolute;
	top: 0;
	left: 0;
	content: "";
	width: 100%;
	height: 100%;
	border-radius: 5px;
	background-color: #b1005d;
	opacity: 0.0;
	-moz-transition: opacity 0.15s linear;
	-webkit-transition: opacity 0.15s linear;
	-o-transition-property: opacity 0.15s linear;
	-ms-transition-property: opacity 0.15s linear;
	transition-property: opacity 0.15s linear;
}
.fl-node-<?php echo $id; ?> .item:hover .img:after, .fl-node-<?php echo $id; ?> .item:hover .img:after, .fl-node-<?php echo $id; ?> .item:hover .img:after{
<!-- 	opacity: 0.7; -->
}
.fl-node-<?php echo $id; ?> .item .content, .fl-node-<?php echo $id; ?> .item .content, .fl-node-<?php echo $id; ?> .item .content{
	float: left;
	width: calc(100% - 214px);
    margin-left: 31px;
}
.fl-node-<?php echo $id; ?> .item .content p, .fl-node-<?php echo $id; ?> .item .content p, .fl-node-<?php echo $id; ?> .item .content p, .fl-node-<?php echo $id; ?> .item .content a, .fl-node-<?php echo $id; ?> .item .content a, .fl-node-<?php echo $id; ?> .item .content a{
	margin: 0;
}
.fl-node-<?php echo $id; ?> .item:hover a.title{
	text-decoration: underline;
}
.fl-node-<?php echo $id; ?> .item .content p.title, .fl-node-<?php echo $id; ?> .item .content p.title, .fl-node-<?php echo $id; ?> .item .content p.title, .fl-node-<?php echo $id; ?> .item .content a.title, .fl-node-<?php echo $id; ?> .item .content a.title, .fl-node-<?php echo $id; ?> .item .content a.title{
	font-family:'Helvetica Neue LT W01_75 Bold';
	font-size: 1.5em;
	margin-bottom: 0.55em;
	display: block;
}
.fl-node-<?php echo $id; ?> .item .content p.title:hover, .fl-node-<?php echo $id; ?> .item .content p.title:hover, .fl-node-<?php echo $id; ?> .item .content p.title:hover, .fl-node-<?php echo $id; ?> .item .content a.title:hover, .fl-node-<?php echo $id; ?> .item .content a.title:hover, .fl-node-<?php echo $id; ?> .item .content a.title:hover{
	text-decoration: underline;
}
.fl-node-<?php echo $id; ?> .item .content p.by, .fl-node-<?php echo $id; ?> .item .content p.by, .fl-node-<?php echo $id; ?> .item .content p.by, .fl-node-<?php echo $id; ?> .item .content a.by, .fl-node-<?php echo $id; ?> .item .content a.by, .fl-node-<?php echo $id; ?> .item .content a.by{
	opacity: 0.6;
	font-size: 1em;
}
.fl-node-<?php echo $id; ?> .item .content p.by span, .fl-node-<?php echo $id; ?> .item .content p.by span, .fl-node-<?php echo $id; ?> .item .content p.by span, .fl-node-<?php echo $id; ?> .item .content a.by span, .fl-node-<?php echo $id; ?> .item .content a.by span, .fl-node-<?php echo $id; ?> .item .content a.by span{
}
.fl-node-<?php echo $id; ?> .item .content p.by span.category, .fl-node-<?php echo $id; ?> .item .content p.by span.category, .fl-node-<?php echo $id; ?> .item .content p.by span.category, .fl-node-<?php echo $id; ?> .item .content a.by span.category, .fl-node-<?php echo $id; ?> .item .content a.by span.category, .fl-node-<?php echo $id; ?> .item .content a.by span.category{
	background-repeat: no-repeat;
	background-position: left center;
}
.fl-node-<?php echo $id; ?> .item .content p.by span.category.video, .fl-node-<?php echo $id; ?> .item .content p.by span.category.video, .fl-node-<?php echo $id; ?> .item .content p.by span.category.video, .fl-node-<?php echo $id; ?> .item .content a.by span.category.video, .fl-node-<?php echo $id; ?> .item .content a.by span.category.video, .fl-node-<?php echo $id; ?> .item .content a.by span.category.video{
	background-image: url(/wp-content/themes/celerion/img/video_icon.png);
}
.fl-node-<?php echo $id; ?> .item .content p.by span.category.pdf, .fl-node-<?php echo $id; ?> .item .content p.by span.category.pdf, .fl-node-<?php echo $id; ?> .item .content p.by span.category.pdf, .fl-node-<?php echo $id; ?> .item .content a.by span.category.pdf, .fl-node-<?php echo $id; ?> .item .content a.by span.category.pdf, .fl-node-<?php echo $id; ?> .item .content a.by span.category.pdf{
	background-image: url(/wp-content/themes/celerion/img/pdf_icon.png);
}
.fl-node-<?php echo $id; ?> .item .content p.by span.category.whitepaper, .fl-node-<?php echo $id; ?> .item .content p.by span.category.whitepaper, .fl-node-<?php echo $id; ?> .item .content p.by span.category.whitepaper, .fl-node-<?php echo $id; ?> .item .content a.by span.category.whitepaper, .fl-node-<?php echo $id; ?> .item .content a.by span.category.whitepaper, .fl-node-<?php echo $id; ?> .item .content a.by span.category.whitepaper{
	background-image: url(/wp-content/themes/celerion/img/whitepaper_icon.png);
}
.fl-node-<?php echo $id; ?> .page_list a, .fl-node-<?php echo $id; ?> .resource_holder a, .fl-node-<?php echo $id; ?> .news_holder a{
}
.fl-node-<?php echo $id; ?> .page_list a.more, .fl-node-<?php echo $id; ?> .resource_holder a.more, .fl-node-<?php echo $id; ?> .news_holder a.more{
	font-family: 'Helvetica Neue LT W01_75 Bold';
	font-size: 1.13em;
	margin-top: 1.25em;
	float: right;
}
.fl-node-<?php echo $id; ?> .page_list a.more i, .fl-node-<?php echo $id; ?> .resource_holder a.more i, .fl-node-<?php echo $id; ?> .news_holder a.more i{
	color: #b30560;
	margin-left: 0.3em;
	-moz-transition: all 0.15s linear;
	-webkit-transition: all 0.15s linear;
	-o-transition-property: all 0.15s linear;
	-ms-transition-property: all 0.15s linear;
	transition-property: all 0.15s linear;
}

.fl-node-<?php echo $id; ?> .page_list a.more:hover, .fl-node-<?php echo $id; ?> .resource_holder a.more:hover, .fl-node-<?php echo $id; ?> .news_holder a.more:hover{
	text-decoration: underline;
}

.fl-node-<?php echo $id; ?> .page_list a.more:hover i, .fl-node-<?php echo $id; ?> .resource_holder a.more:hover i, .fl-node-<?php echo $id; ?> .news_holder a.more:hover i{
	font-weight: 600;
}

@media only screen and (min-width: 768px) {
	.fl-node-<?php echo $id; ?> .page_list.horizontal h4{
		border-bottom: 0px;
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item a.title{
		word-wrap: break-word;
		hyphens: auto;
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item a.img{
		width: 140px;
		height: 95px;
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item .content{
		width: calc(100% - 171px);
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item{
		width: calc(96%/<?php echo $settings->num; ?>);
		margin-right: calc(4%/<?php echo $settings->num; ?>);
		float: left;
		border-bottom: 0px;
		border-top: 1px solid #232827;
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item:last-child{
		margin-right: 0;
	}
}

@media only screen and (min-width: 1000px) {
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item a.title{
		word-wrap: none;
		hyphens: none;
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item a.img{
		width: 183px;
		height: 123px;
	}
	.fl-node-<?php echo $id; ?> .page_list.horizontal .item .content{
		width: calc(100% - 214px);
	}
}