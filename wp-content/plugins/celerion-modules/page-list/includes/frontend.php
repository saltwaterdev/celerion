<?php 

$pageid = url_to_postid($settings->parent); 
$currentid = get_the_ID();
if ($settings->num){
	$num = $settings->num;
} else {
	$num = 3;
}
if ($pageid == 2992){
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => array('post', 'event'), 'posts_per_page' => $num, 'tag' => $settings->tag));
	if (count($all_wp_pages) == 0){
		$all_wp_pages = $my_wp_query->query(array('post_type' => array('post', 'event'), 'posts_per_page' => $num));
	}
} else if ($pageid == 4650){
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => array('post'), 'cat' => 32, 'posts_per_page' => $num, 'tag' => $settings->tag, 'orderby' => 'date', 'order' => 'DESC'));
} else if ($pageid == 4653){
	$my_wp_query = new WP_Query();
	$querySearch = array(
		'post_type' => 'event',
	    'meta_key' => 'conference_start',
	    'meta_compare' => '>=',
	    'meta_value' => date ('Y-m-d'),
	    'meta_type'  => 'DATE',
	    'post_status' => 'publish',
	    'posts_per_page' => $num,
	    'orderby' => 'meta_value',
	    'order' => 'ASC'
	);
	$all_wp_pages = $my_wp_query->query($querySearch);
} else if ($pageid == 4656){
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => array('post'), 'cat' => 27, 'posts_per_page' => $num, 'tag' => $settings->tag, 'orderby' => 'date', 'order' => 'DESC'));
} else if ($pageid == 3078 || $pageid == 3076){
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'resource', 'posts_per_page' => $num, 'tag' => $settings->tag));
	if (count($all_wp_pages) == 0){
		$all_wp_pages = $my_wp_query->query(array('post_type' => 'resource', 'posts_per_page' => $num));
	}
} else {
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'any', 'post_parent' => $pageid, 'posts_per_page' => $num, 'tag' => $settings->tag));
}
?>

<div class="page_list <?php if ($settings->template == 2){ echo "horizontal";}; ?>">
   <h4><?php echo $settings->title; ?></h4>
   <div class="item-container">
   <?php foreach ($all_wp_pages as $page){ ?>
   		<div class="item">
	   		<?php 
		   	
		   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
		   		$style = "contain";	
		   		if (get_post_type($page->ID) == "resource"){
			   		$terms = get_the_terms($page->ID, 'type');
			   		if ( $terms && ! is_wp_error( $terms ) ){
						foreach ( $terms as $term ) {
							if ($term->slug == "articles"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/article.png); background-size: 55%;"';
							} else if ($term->slug == "case-study"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/case_study.png); background-size: 55%;"';
							} else if ($term->slug == "presentation"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/presentation.png); background-size: 55%;"';
							} else if ($term->slug == "recorded-webex"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
							} else if ($term->slug == "scientific-poster"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/scientific_poster.png); background-size: 55%;"';
							} else if ($term->slug == "video"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/video.png); background-size: 55%;"';
		    				} else if ($term->slug == "webinar"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
		    				} else if ($term->slug == "white-papers"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/whitepaper.png); background-size: 55%;"';
		    				} else if ($term->slug == "fact-sheet"){
		    					$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/fact-sheet-icon.png); background-size: 55%;"';
		    				} else{
			    				$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
		    				}
						}
					} else {
						$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
					}
			   	} else {
				   	if ($thumb == 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"'){
					   	if (get_post_type($page->ID) == "event"){
						   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/event.png);"';
					   	} else {
						   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/news.png);"';
					   	}
				   	}
			   	}	
			   	if ($pageid == 4656){
				   	$thumb = 'style="background-image: url(/wp-content/themes/celerion/img/icons/blog.png); background-size: 50%;"';
			   	}
		   		
		   	?>
        	<a href="<?php echo get_permalink($page->ID); ?>" class="img" <?php echo $thumb; ?>></a>
			<div class="content">
				<?php
                                	$words = str_word_count(get_the_content());
				$wpm = 100; // Average screen words read per minute
				$minutes =ceil($words/$wpm);
					$target = "";
					if (get_post_type($page->ID) == "resource"){
						$pagelink = get_permalink($page->ID);
						$resourceinfo = get_post_meta($page->ID);
						if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
							$pagelink = $resourceinfo['resource_file'][0];
							$target = 'target="_blank"';
						}
						if ($resourceinfo['external_link'][0] && $resourceinfo['external_link'][0] !== ""){
							$pagelink = $resourceinfo['external_link'][0];
							$target = 'target="_blank"';
						}
					} else {
						$pagelink = get_permalink($page->ID);
						$resourceinfo = get_post_meta($page->ID);
						if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
							$pagelink = $resourceinfo['resource_file'][0];
							$target = 'target="_blank"';
						}
						if ($resourceinfo['external_link'][0] && $resourceinfo['external_link'][0] !== ""){
							$pagelink = $resourceinfo['external_link'][0];
							$target = 'target="_blank"';
						}
					}
				?>
            	<a href="<?php echo $pagelink; ?>" class="title" <?php echo $target; ?>><?php echo get_the_title($page->ID); ?></a>
            	<?php if (get_post_type($page->ID) == "event"){ $eventinfo = get_post_meta( $page->ID ); ?>
	            	<?php 
				        if ( isset ( $eventinfo['conference_start'] ) ){
				        	$startdatearray = explode("-", $eventinfo['conference_start'][0]);
				        }
				        if ( isset ( $eventinfo['conference_end'] ) ){
				        	$enddatearray = explode("-", $eventinfo['conference_end'][0]);
				        }
			        ?>
            		<p class="by"><span class="date"><?php if ( isset ( $eventinfo['conference_start'] ) ) echo $startdatearray[1].'.'.$startdatearray[2].'.'.$startdatearray[0]; ?> <?php if ( isset ( $eventinfo['conference_end'] ) ) echo ' - '. $enddatearray[1].'.'.$enddatearray[2].'.'.$enddatearray[0]; ?></span> <!-- - <span class="name">By John Smith</span> --></p>
            	<?php } else if (get_post_type($page->ID) == "resource" || get_the_category($page->ID) == "news"){ ?>
	            	<?php
						$terms = get_the_terms($page->ID, 'type');
						$types = "";
						$termscount = count($terms);
						$termcount = 1;
						if ( $terms && ! is_wp_error( $terms ) ){
							foreach ( $terms as $term ) {
								$types .= $term->name;
								if ($termcount !== $termscount){
									$types .= " | ";
								}
								$termcount++;
							}
						}
                                              if ($settings->include_readtime == 1){ 
                                                  ?>
                <div class="meta">
                                                  <span class="time"><?= $minutes ?> Min.</span>
                                                  <?php
                                              } 
                                              
                                             if ($settings->include_socialShare == 1){ 
                                             
                                                 ?>
                                                 <span class="share" tabindex="0">
	<div class="shareTitle">
		Share
	</div>
	<div class="shareBox">
		<?php
			$pageLink = urlencode(get_page_link());
		?>
		<a href="https://pinterest.com/pin/create/button/?url=<?= $pageLink ?>&description=" target="_blank"><i class="fab fa-pinterest-p"></i></a>
		<a href="https://twitter.com/home?status=<?= $pageLink ?>" target="_blank"><i class="fab fa-twitter"></i></a>
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?= $pageLink ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
		<a href="https://plus.google.com/share?url=<?= $pageLink ?>" target="_blank"><i class="fab fa-google-plus-g"></i></a>
		<a href="http://www.tumblr.com/share?v=3&u=<?= $pageLink ?>&t=" target="_blank"><i class="fab fa-tumblr"></i></a>
	</div>
</span>
                                                  </div>
                                                 <?php
                                                 
                                             }
					
                if ($settings->include_by == 1){
                    ?>
                                                  <p class="by"><span class="date"><?php echo $types; ?></span> <!-- - <span class="name">By John Smith</span> --></p>
                <?php
                } 
            		
				 } else { 
                                     if ($settings->include_readtime == 1 && $minutes > 0){ 
                                                  ?>
                <div class="meta">
                                                  <span class="time"><?= $minutes ?> Min.</span>
                                                  <?php
                                              } 
                                              
                                             if ($settings->include_socialShare == 1){ 
                                             
                                                 ?>
                                                 <span class="share" tabindex="0">
	<div class="shareTitle">
		Share
	</div>
	<div class="shareBox">
		<?php
			$pageLink = urlencode(get_page_link());
		?>
		<a href="https://pinterest.com/pin/create/button/?url=<?= $pageLink ?>&description=" target="_blank"><i class="fab fa-pinterest-p"></i></a>
		<a href="https://twitter.com/home?status=<?= $pageLink ?>" target="_blank"><i class="fab fa-twitter"></i></a>
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?= $pageLink ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
		<a href="https://plus.google.com/share?url=<?= $pageLink ?>" target="_blank"><i class="fab fa-google-plus-g"></i></a>
		<a href="http://www.tumblr.com/share?v=3&u=<?= $pageLink ?>&t=" target="_blank"><i class="fab fa-tumblr"></i></a>
	</div>
</span>
                                                  </div>
                  <?php
                                                 
                                             }
                                     if ($settings->include_date == 1){ 
                                     ?>
                
					<p class="by"><span class="date"><?php echo get_the_date( 'm.d.y', $page->ID ); ?></span> <!-- - <span class="name">By John Smith</span> --></p>
				<?php 
                                 }
                                
                                             } ?>
        	</div>
    	</div>
   <?php } ?>
   </div>
   <?php if ($settings->include_link == 1){ ?>
   	 	<a href="<?php echo $settings->parent; ?>" class="more">All <?php echo $settings->title; ?> <i class="fal fa-chevron-circle-right"></i></a>
    <?php } ?>
</div>