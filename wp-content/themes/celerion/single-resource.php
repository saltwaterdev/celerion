<?php get_header(); ?>
	
	<div class="container post">
		<?php
	    while ( have_posts() ) : the_post(); ?>
	        <div class="entry-content-page post">
		        <h1><?php echo the_title(); ?></h1>
		        <?php
					$terms = get_the_terms(get_the_ID(), 'type');
					$types = "";
					$termscount = count($terms);
					$termcount = 1;
					if ( $terms && ! is_wp_error( $terms ) ){
						foreach ( $terms as $term ) {
							$types .= $term->name;
							if ($termcount !== $termscount){
								$types .= " | ";
							}
							$termcount++;
						}
					}	
				?>
				<?php if ($types !== ""){ ?><p class="type byline"><?php echo $types; ?></p><?php } ?>
		        <?php if ( has_post_thumbnail() ) { 
		         echo '<div class="img" style="background-image: url('.get_the_post_thumbnail_url(get_the_ID()).')"></div>';
		        } ?>
	            <?php the_content(); ?>
	            <div class="info">
		        <?php
					$posttags = get_the_tags();
					if ($posttags) { ?>
						<div class="tags">
							<p>Tagged in:</p>
							<?php
							foreach($posttags as $tag) {
								echo '<a href="#">'.$tag->name.'</a>';
							}
							?>
						</div>
						<?php
						}
						?>
		            <div class="sharethis-inline-share-buttons"></div>
	            </div>
	        </div>
	
	    <?php
	    endwhile;
	    wp_reset_query();
	    ?>
	</div>
<?php  get_footer(); ?>