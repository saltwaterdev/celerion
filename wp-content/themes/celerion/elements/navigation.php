<div id="nav_holder">
	<a href="#" class="menu_close"><i class="fas fa-times"></i></a>
	<div id="utility_nav">
		<a href="http://helpresearch.com/" class="button black" target="_blank">Participate in a Study</a>
		<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'Utility Navigation', 'celerion' ),  // nav name
			'menu_class' => 'nav utility_nav cf',               // adding custom nav class
			'theme_location' => 'utility_nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
		)); ?>
	</div>
	<div id="main_nav">
		<a href="#" class="search"><i class="fas fa-search"></i></a>
		<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'Main Navigation', 'celerion' ),  // nav name
			'menu_class' => 'nav main_nav cf',               // adding custom nav class
			'theme_location' => 'main_nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
		)); ?>
	</div>
	<div id="utility_nav_mobile">
		<?php wp_nav_menu(array(
			'container' => false,                           // remove nav container
			'container_class' => 'menu cf',                 // class of container (should you choose to use it)
			'menu' => __( 'Utility Navigation', 'celerion' ),  // nav name
			'menu_class' => 'nav utility_nav cf',               // adding custom nav class
			'theme_location' => 'utility_nav',                 // where it's located in the theme
			'before' => '',                                 // before the menu
			'after' => '',                                  // after the menu
			'link_before' => '',                            // before each link
			'link_after' => '',                             // after each link
			'depth' => 0,                                   // limit the depth of the nav
			'fallback_cb' => ''                             // fallback function (if there is one)
		)); ?>
		<a href="http://helpresearch.com/" target="_blank" class="button black">Participate in a Study</a>
	</div>
	<?php get_search_form(); ?>
</div>