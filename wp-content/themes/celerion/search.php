<?php get_header(); ?>
	<?php
		global $wp_query;
		$total_results = $wp_query->found_posts;
		$search_query = get_search_query();
	?>
	<div class="search_results">
		<div class="left">
			<hr>
			<h1>(<?php echo $total_results; ?>) Results for "<?php echo $search_query ?>"</h1>
		</div>
		<div class="right">
			<hr>
			<?php if ($total_results == 0){ ?>
				<h4>Sorry, we weren't able to find any results for "<?php echo $search_query; ?>". Check to be sure your search is free of typos, or try searching for a similar term so we can help find what you're looking for.</h4>
			<?php } else { $resultcount = 1; ?>
				<?php while ( have_posts() ) : the_post(); ?>
				<div class="holder<?php if ($resultcount > 6){echo " hide";}; ?>">
					<?php 
			            $target = "";
			        	if (get_post_type(get_the_ID()) == "resource"){  
				        	$resourceinfo = get_post_meta(get_the_ID());
				        	if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
								$thelink = $resourceinfo['resource_file'][0];
								$target = 'target="_blank"';
							} else {
								$thelink = get_permalink(get_the_ID());
							}
				        } else {
					        $thelink = get_permalink(get_the_ID());
				        }
			        ?>
		            <h4><a href="<?php echo $thelink; ?>" class="title" <?php echo $target; ?>><?php the_title(); ?></a></h4>
		            <?php the_excerpt(); ?>
		            <a href="<?php echo $thelink; ?>" class="search-post-link" <?php echo $target; ?>><?php echo $thelink; ?></a>
		            <hr>
				</div>
	            <?php $resultcount++; endwhile; ?>
            <?php } ?>
            <?php if ($total_results > 6){ ?> <a href="#" class="more">Load More Results <i class="fal fa-chevron-circle-down"></i></a><?php } ?>
		</div>
	</div>
<?php  get_footer(); ?>