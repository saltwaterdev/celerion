<footer>
	<div class="container">
		<div class="col">
			<?php if ( is_active_sidebar( 'footer_col_one' ) ) : 
					dynamic_sidebar( 'footer_col_one' );
				endif;
			?>
		</div>
		<div class="col">
			<?php if ( is_active_sidebar( 'footer_col_two' ) ) : 
					dynamic_sidebar( 'footer_col_two' );
				endif;
			?>
		</div>
		<div class="col">
			<?php if ( is_active_sidebar( 'footer_col_three' ) ) : 
					dynamic_sidebar( 'footer_col_three' );
				endif;
			?>
                    
                    <p class='copyRight'>&copy; Celerion <?php echo date("Y"); ?>. All Rights Reserved.   <a href="/privacy-policy/">Privacy & Legal</a></p>
		</div>

	</div>
</footer>

<?php wp_footer(); ?>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.3/iframeResizer.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>

</body>
</html>