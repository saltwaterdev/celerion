jQuery(document).ready(function($) {
	if($("#alert_banner")[0]){
		bannerHeight = $("#alert_banner").outerHeight();
		$("#alert_banner").attr("data-height",bannerHeight);
	}
	$( "#alert_banner a.close" ).click(function(e) {
		e.preventDefault();
		distanceToMove = $("#alert_banner").outerHeight() * -1;
		$("#alert_banner").animate({
			marginTop: distanceToMove
		}, 150, function() {
			$("#alert_banner").remove();
			var now = new Date();
			now.setTime(now.getTime() + 1 * 3600 * 1000 * 24);
			document.cookie = "alert=alert; expires=" + now.toUTCString() + "; path=/";
		});
	});
	
	$("p,h1,h2,h3,h4,li,a").each(function(){
	    $(this).html($(this).html().replace(/&reg;/gi, '<sup>&reg;</sup>').replace(/®/gi, '<sup>&reg;   </sup>').replace(/&copy;/gi, '<sup>&copy;</sup>').replace(/©/gi, '<sup>&copy;   </sup>').replace(/&trade;/gi, '<sup>&trade;</sup>').replace(/™/gi, '<sup>&trade;   </sup>'));
	});
	
	$(".menu-item").each(function(){
		htmlText = $(this).children("a").text();
		$(this).children("a").text(htmlText.replace(/&reg;/gi, '<sup>&reg;</sup>').replace(/®/gi, '<sup>&reg;   </sup>').replace(/&copy;/gi, '<sup>&copy;</sup>').replace(/©/gi, '<sup>&copy;   </sup>').replace(/&trade;/gi, '<sup>&trade;</sup>').replace(/™/gi, '<sup>&trade;   </sup>'));
	});
	
	if ($(".content.innovation")[0]){
		$(".content.innovation").each(function( index ) {
			$(this).parent().parent().parent().parent(".fl-col").addClass("lp");
		});
	}
	
	setTimeout(function(){
		formCount = 0;
		$( ".one-half" ).each(function( index ) {
			if (formCount % 2 === 0 || formCount == 0) {
				$(this).addClass("nm");
			}
			formCount++;
		});
		$( ".one-half-container" ).each(function( index ) {
			if (formCount % 2 === 0 || formCount == 0) {
				$(this).addClass("nm");
			}
			formCount++;
		});
	}, 250);

	if(window.location.hash == "#why") {
		$('html, body').animate({
			scrollTop: $("#why").offset().top
		}, 500, 'linear');
	} 
	$('input[type=radio]').change(function() {
	    $('input[type=radio]').removeClass("checked");
	    $(this).addClass("checked");
	});
	$( "iframe" ).each(function( index ) {
		if ($(this).attr("src").indexOf("youtube") > -1){
			$(this).wrap('<div class="video_wrapper"></div>');
		}
	});
	$( "#main_nav .sub-menu" ).each(function( index ) {
		if ($(window).width() < 968) {
			$(this).prepend('<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-mobile"><a href="'+$(this).siblings("a").attr("href")+'" class="">Overview</a></li>');
		}
	});
	$( "header #main_nav .menu-item .sub-menu a").each(function( index ) {
		$(this).html('<span>'+$(this).text()+'</span>');
	});
	$( "header #main_nav .menu-item .sub-menu .sub-menu").addClass("none");
	$( "header #main_nav .menu-item a:first-child").addClass("menu");
	$( "header #main_nav .menu-item .sub-menu .menu-item a:first-child").removeClass("menu");
	$( "header #main_nav .menu-item a.menu" ).mouseover(function() {
		if ($(window).width() >= 968 && !$( "#menu-main-nav" ).hasClass("stop") && !$(this).parent("li").hasClass("hover")) {
			$( "#menu-main-nav" ).addClass("stop");
			$("header #main_nav .menu-item.hover ul.sub-menu").addClass("close");
// 			$("header #main_nav .menu-item.hover").removeClass("hover");
			$(this).parent(".menu-item").addClass("hover");
			$( "header #main_nav .menu-item a:first-child").addClass("target");
			$( "header #main_nav .menu-item .sub-menu .menu-item a:first-child").removeClass("target");
			$(this).parent(".menu-item a:first-child").removeClass("target");
			if ($(this).next("ul.sub-menu")[0]){
				subMenu = $(this).next("ul.sub-menu");
				subMenu.show().css("opacity",0);
				menuHeight = subMenu.outerHeight();
				subMenu.css("height","0").css("opacity",1).animate({
					height: menuHeight
				}, 150, function() {
					$( "#menu-main-nav" ).removeClass("stop");
				});
			} else {
				$( "#menu-main-nav" ).addClass("stop");
				setTimeout(function(){
					$( "#menu-main-nav" ).removeClass("stop");
				}, 150);
			}
			$("header #main_nav .menu-item.hover ul.sub-menu.close").animate({
				height: 0
			}, 75, function() {
				$("header #main_nav .menu-item ul.sub-menu.close").parent("li").removeClass("hover");
				$("header #main_nav .menu-item ul.sub-menu.close").attr("style","").removeClass("close");
			});
		}
		if ($(window).width() >= 968 && $(this).text() == "Resources") {
			$( "#menu-main-nav" ).addClass("stop");
			$("header #main_nav .menu-item.hover ul.sub-menu").addClass("close");
			$("header #main_nav .menu-item.hover ul.sub-menu.close").animate({
				height: 0
			}, 75, function() {
				$("header #main_nav .menu-item ul.sub-menu.close").parent("li").removeClass("hover");
				$("header #main_nav .menu-item ul.sub-menu.close").attr("style","").removeClass("close");
			});
			setTimeout(function(){
				$( "#menu-main-nav" ).removeClass("stop");
			}, 150);
		}
	});
	$( "header #main_nav .menu-item .sub-menu li.menu-item" ).mouseover(function() {
		if ($(window).width() >= 968 && !$( "#menu-main-nav" ).hasClass("stop")) {
			$( "header #main_nav .menu-item .sub-menu li.menu-item a" ).removeClass("subopen");
			if ($(this).hasClass("menu-item-has-children")){
				$(this).children("a").addClass("subopen");
			}
		}
	});	
	$( "header #main_nav .menu-item a.menu" ).click(function(e) {
		if ($(window).width() < 968) {
			if ($(this).parent("li").hasClass("menu-item-has-children")){
				e.preventDefault();
				$(this).toggleClass("opened");
				$("header #main_nav .menu-item.hover").removeClass("hover");
				$(this).parent(".menu-item").addClass("hover");
				$( "header #main_nav .menu-item a:first-child").addClass("target");
				$( "header #main_nav .menu-item .sub-menu .menu-item a:first-child").removeClass("target");
				$(this).parent(".menu-item a:first-child").removeClass("target");
			} 
			if (!$(this).hasClass("opened") && $(this).parent("li").hasClass("menu-item-has-children")){
				$("header #main_nav .menu-item").removeClass("hover");
				$( "header #main_nav .menu-item .sub-menu .menu-item a:first-child").removeClass("target");
			}
		}
	});
	function formSize(){
		setTimeout(function(){
			$(".one-half").parent("nf-field").addClass("one-half-container");
			$( ".one-half-container" ).each(function( index ) {
				if (formCount % 2 === 0 || formCount == 0) {
					$(this).addClass("nm");
				}
				formCount++;
			});
		}, 250);
	}
	formSize();
	$("body").on("click",".nf-next",function(e) {
		formSize();
	});
	$( ".sub-menu li.menu-item-has-children a" ).click(function(e) {
		if ($(window).width() < 968) {
			if ($(this).parent("li").hasClass("menu-item-has-children")){
				e.preventDefault();
			}
			$(this).toggleClass("opennav");
			if (!$(this).hasClass("opennav")){
				$(this).mouseout();
				$(this).siblings(".sub-menu").hide();
			} else {
				$(this).mouseover();
				$(this).siblings(".sub-menu").show();
			}
// 			$(this).siblings("ul.sub-menu").remove();
		}
	});
	$( ".search_results a.more" ).click(function(e) {
		e.preventDefault();
		for(x=0;x<6;x++){
			$( ".search_results .holder.hide").eq(x).addClass("expander");
		}
		$( ".search_results .holder.expander").slideToggle( "fast", function() {
			$( ".search_results .holder.expander").removeClass("hide").removeClass("expander");
		});
	});
	$( ".news_holder a.more" ).click(function(e) {
		e.preventDefault();
		for(x=0;x<8;x++){
			$( ".news_holder .item.hide").eq(x).addClass("outgoing").show().addClass("activeitem");
		}
		$('.news_holder .resources').isotope({ filter: '.activeitem'});
		setTimeout(function(){
			$( ".news_holder .item.outgoing").removeClass("hide").removeClass("outgoing");
			$('.news_holder .resources').isotope({ filter: '.activeitem'});
		}, 500);
/*
		$( ".news_holder .item.expander").slideToggle( "fast", function() {
			$( ".news_holder .item.expander").removeClass("hide").removeClass("expander");
		});
*/
	});
	$( ".resource_holder a.more" ).click(function(e) {
		e.preventDefault();
		for(x=0;x<8;x++){
			$( ".resource_holder .item.hide").eq(x).addClass("outgoing").show().addClass("activeitem");
		}
/*		$( ".resource_holder .item.expander").slideToggle( "fast", function() {

			$( ".resource_holder .item.expander").removeClass("hide").removeClass("expander").addClass("activeitem");
		});
*/
		$('.resource_holder .resources').isotope({ filter: '.activeitem'});
		setTimeout(function(){
			$( ".resource_holder .item.outgoing").removeClass("hide").removeClass("outgoing");
			$('.resource_holder .resources').isotope({ filter: '.activeitem'});
		}, 500);
	});
	$( "header a.menu_open" ).click(function(e) {
		e.preventDefault();
		$(".search_form_holder").attr("style","");
		if ($(window).width() < 968) {
			$("#nav_holder").show().animate({
				opacity: "1"
			}, 150, function() {
			});
		}
	});
	$( "header a.menu_close" ).click(function(e) {
		e.preventDefault();
		if ($(window).width() < 968) {
			$("#nav_holder").animate({
				opacity: "0"
			}, 150, function() {
				$("#nav_holder").hide();
			});
		}
	});
	$( "#menu-main-nav" ).mouseleave(function() {
		if ($(window).width() >= 968) {
			$( "#menu-main-nav" ).addClass("stop");
// 			$("header #main_nav .menu-item").removeClass("hover");
			$("ul.sub-menu").animate({
				height: 0
			}, 150, function() {
				$("ul.sub-menu").attr("style","")
				$("header #main_nav .menu-item").removeClass("hover");
				$( "header #main_nav .menu-item a:first-child").removeClass("target");
				$( "#menu-main-nav" ).removeClass("stop");
				$( "header #main_nav .menu-item .sub-menu li.menu-item-has-children a" ).removeClass("subopen");
			});
		}
	});
	$("body").on("click","header #main_nav .search, .search_form .close",function(e) {
		e.preventDefault();
		toggleSearch();
	});
	$("body").on("keyup",".assay_search .search_box",function(e) {
		searchVal = $(this).val().length;
		if (searchVal < 1){
			$("table.assay.searchable tbody tr").show();
			$("table.assay.searchable").show();
			$("table.assay.searchable").parent(".tableholder").find("h4").remove();
		}
	});
	$( ".assay_search #filterby" ).change(function() {
		if ($(this).val() == 1){
			$("table.assay.searchable tbody tr").show();
		} else {
			for(x=0;x<$("table.assay.searchable tbody tr").length;x++){
				searchRow = false;
				elem = $("table.assay.searchable tbody tr").eq(x);
				searchVal = $(".search_box").val().toLowerCase();
				if (searchVal.length > 0 && $(this).val() !== 1){
					if (searchVal.length > 0){
						for(y=0;y<elem.children("td").length;y++){
							findText = elem.children("td").eq(y).text().toLowerCase().replace(" ","");
							if (findText.indexOf(searchVal) > -1 && elem.children("td").eq(1).text() == $(this).val()){
								searchRow = true;
							}
						}
					}
				} else {
					if (searchVal.length > 0){
						for(y=0;y<elem.children("td").length;y++){
							findText = elem.children("td").eq(y).text().toLowerCase().replace(" ","");
							if (findText.indexOf(searchVal) > -1){
								searchRow = true;
							}
						}
					}
					if (elem.children("td").eq(1).text() == $(this).val()){
						searchRow = true;
					}
				}
				if (searchRow == true){
					elem.show().addClass("active");
				} else {
					elem.hide().removeClass("active");
				}
			}
		}
		$('html, body').animate({
			scrollTop: $("#validated").offset().top
		}, 500, 'linear');
		for(z=0;z<$("table.assay.searchable").length;z++){
			numActive = 0;
			numActive = $("table.assay.searchable").eq(z).find("tr.active").length;
			if (numActive < 1){
				$("table.assay.searchable").eq(z).parent(".tableholder").find("h4").remove();
				$("table.assay.searchable").eq(z).hide();
				if (z == 0){
					$("table.assay.searchable").eq(z).parent(".tableholder").prepend("<h4>Sorry, we weren't able to find any validated assay results for '"+$(".search_box").val()+"'. Check to be sure your search is free of typos, or try searching with a single word to narrow down the assay list to what you're looking for.</h4>");
				}
				if (z == 1){
					$("table.assay.searchable").eq(z).parent(".tableholder").prepend("<h4>Sorry, we weren't able to find any under-development assay results for '"+$(".search_box").val()+"'. Check to be sure your search is free of typos, or try searching with a single word to narrow down the assay list to what you're looking for.</h4>");
				}
			} else {
				$("table.assay.searchable").eq(z).show();
				$("table.assay.searchable").eq(z).parent(".tableholder").find("h4").remove();
			}
		}
	});
	$( ".search_form" ).submit(function( event ) {
		event.preventDefault();
		searchVal = $( ".search_form" ).children("#search").val();
		searchFind = searchVal.indexOf("phase");
		if (searchFind > -1){
			searchVal = searchVal.replace("phase iii", "phase 3").replace("phase ii", "phase 2").replace("phase i", "phase 1").replace("phase III", "phase 3").replace("phase II", "phase 2").replace("phase I", "phase 1").replace("phaseiii", "phase 3").replace("phaseii", "phase 2").replace("phasei", "phase 1").replace("phaseIII", "phase 3").replace("phaseII", "phase 2").replace("phaseI", "phase 1");
		}
		searchVal = searchVal.toLowerCase().replace(" ","+").replace("'","").replace('"',"");
		urlGo = window.location.origin+"/?s="+searchVal;
		window.location.href = urlGo;
	});
	$( ".assay_search" ).submit(function( event ) {
		event.preventDefault();
		searchVal = $(".search_box").val().toLowerCase();
		liveCount = 0;
		if (searchVal.length > 0){
			$("table.assay.searchable tbody tr").hide().removeClass("active");
			for(x=0;x<$("table.assay.searchable tbody tr").length;x++){
				searchRow = false;
				elem = $("table.assay.searchable tbody tr").eq(x);
				for(y=0;y<elem.children("td").length;y++){
					findText = elem.children("td").eq(y).text().toLowerCase().replace(" ","");
					if (findText.indexOf(searchVal) > -1){
						elem.show().addClass("active");	
					}
				}
				if ($( ".assay_search #filterby" ).find("option:selected").val() !== "1"){
					if (elem.children("td").eq(1).text() !== $( ".assay_search #filterby" ).find("option:selected").val()){
						elem.hide().removeClass("active");	
					}
				}
/*
				if (searchRow == true){
					elem.show().addClass("active");	
					liveCount++;
				} else {
					elem.hide();
				}
*/
			}
			$('html, body').animate({
				scrollTop: $("#validated").offset().top
			}, 500, 'linear');
		}
		for(z=0;z<$("table.assay.searchable").length;z++){
			numActive = 0;
			numActive = $("table.assay.searchable").eq(z).find("tr.active").length;
			if (numActive < 1){
				$("table.assay.searchable").eq(z).parent(".tableholder").find("h4").remove();
				$("table.assay.searchable").eq(z).hide();
				if (z == 0){
					$("table.assay.searchable").eq(z).parent(".tableholder").prepend("<h4>Sorry, we weren't able to find any validated assay results for '"+$(".search_box").val()+"'. Check to be sure your search is free of typos, or try searching with a single word to narrow down the assay list to what you're looking for.</h4>");
				}
				if (z == 1){
					$("table.assay.searchable").eq(z).parent(".tableholder").prepend("<h4>Sorry, we weren't able to find any under-development assay results for '"+$(".search_box").val()+"'. Check to be sure your search is free of typos, or try searching with a single word to narrow down the assay list to what you're looking for.</h4>");
				}
			} else {
				$("table.assay.searchable").eq(z).show();
				$("table.assay.searchable").eq(z).parent(".tableholder").find("h4").remove();
			}
		}
	});

	$( ".resource_search #filterby" ).change(function() {
		searchVal = $(".search_box").val().toLowerCase().replace(" ","");
		if ($(this).val() !== 1){
			filterVal = $(this).find("option:selected").text();
			resourceSearch(searchVal,filterVal);
		} else {
			resourceSearch(searchVal,1);
		}
    });
    
    // Filter by 'Covid-19' on page load
	if (window.location.hash == "#covid-19") {

		$('.resource_holder .resources').one('arrangeComplete', function () {
			searchVal = $(".search_box").val().toLowerCase().replace(" ", "");
			$( ".resource_search #filterby" ).val('covid-19');
			resourceSearch(searchVal,'covid-19');
		});
		
    }

	$( ".resource_search" ).submit(function( event ) {
		event.preventDefault();
		searchVal = $(".search_box").val().toLowerCase().replace(" ","");
		if ($( ".resource_search #filterby" ).val() !== 1){
			resourceSearch(searchVal,$( ".resource_search #filterby" ).find("option:selected").text());
		} else {
			resourceSearch(searchVal,1);
		}
    });

	function resourceSearch(searchVal,filterVal){
		$(".resources .item").addClass("activeitem");
		$(".resources .item").each(function( index ) {
			if (searchVal !== ""){
				theTitle = $(this).find("a.title").text().toLowerCase();
				if(theTitle.indexOf(searchVal) < 0){
					$(this).removeClass("activeitem");
				}
			}
			if (filterVal !== "Type"){
				if($(this).find("span.category").text().indexOf(filterVal) < 0){
					$(this).removeClass("activeitem");
				}
			}
		});
		if ($(".activeitem").length > 6){
			$(".item.hide").removeClass("hide");
		}
		$(".activeitem").removeClass("hide");
		$("a.more").hide();
		$('.resource_holder .resources').isotope({ filter: '.activeitem'});

		if ($(".activeitem").length < 1){
			$(".resource_holder a.more").hide();
			$(".resource_holder h3").remove();
			$(".resource_holder").prepend("<h3>Sorry, we weren't able to find any resources results for '"+$(".search_box").val()+"'. Check to be sure your search is free of typos, or try searching for a similar term so we can help find what you're looking for.</h3>")
		} else {
			if ($(".activeitem").length > 6){
// 				$(".resource_holder a.more").show();
			}
			$(".resource_holder h3").remove();
		}

	}
	
	//NEWS SEARCH START
	$( ".news_search #filterby" ).change(function() {
		searchVal = $(".search_box").val().toLowerCase().replace(" ","");
		if ($(this).val() !== 1){
			filterVal = $(this).find("option:selected").val();
			newsSearch(searchVal,filterVal);
		} else {
			newsSearch(searchVal,1);
		}
	});
	$( ".news_search" ).submit(function( event ) {
		event.preventDefault();
		searchVal = $(".search_box").val().toLowerCase().replace(" ","");
		if ($( ".news_search #filterby" ).val() !== 1){
			newsSearch(searchVal,$( ".news_search #filterby" ).find("option:selected").val());
		} else {
			newsSearch(searchVal,1);
		}
	});
	$("body").on("keyup",".news_search .search_box",function(e) {
		searchVal = $(this).val().length;
		if (searchVal < 1){
			newsSearch(searchVal,$( ".news_search #filterby" ).find("option:selected").val());
		}
	});
	function newsSearch(searchVal,filterVal){
		$(".resources .item").addClass("activeitem");
		$(".resources .item").each(function( index ) {
			if (searchVal !== "" && searchVal !== 0){
				theTitle = $(this).find("a.title").text().toLowerCase();
				if(theTitle.indexOf(searchVal) <= 0){
					$(this).removeClass("activeitem");
				}
			}
			if (filterVal !== "1"){
				theTags = $(this).find(".hidden.tags").text();
// 				console.log("Title is "+$(this).find("a.title").text()+".  Index is "+theTags.indexOf(filterVal)+".")
				if (theTags.indexOf(filterVal) < 0){
					$(this).removeClass("activeitem");
				}
			}
		});
		if ($(".activeitem").length > 6){
			$(".item.hide").removeClass("hide");
		}
		$("a.more").hide();
		$('.news_holder .resources').isotope({ filter: '.activeitem'});
		

		if ($(".activeitem").length < 1){
			$(".news_holder a.more").hide();
			$(".news_holder h3").remove();
			$(".news_holder").prepend("<h3>Sorry, we weren't able to find any news & events results for '"+$(".search_box").val()+"'. Check to be sure your search is free of typos, or try searching for a similar term so we can help find what you're looking for.</h3>");
		} else {
			if ($(".activeitem").length > 6){
				$(".news_holder a.more").show();
			}
			$(".news_holder h3").remove();
		}
	}
	
/*
	$("body").on("click",".submit-btn",function(e) {
		e.preventDefault();
		console.log("Working");
		directorName = $(".investigators_form .director_name input[type='text']").val();
		directorNamesplit = directorName.split(" ");
		
		var data = { 'company_name': $(".investigators_form .company_name input[type='text']").val(), 'director_fn': directorNamesplit[0], 'director_ln': directorNamesplit[1], 'email': $(".investigators_form .director_email input[type='email']").val(), 'fax': $(".investigators_form .director_fax input[type='tel']").val(), 'phone': $(".investigators_form .director_phone input[type='tel']").val(), 'areas': $(".investigators_form .areas textarea").val() };
		var uriEncodedParams = $.param(data);
		var targetUrl = 'http://go.celerion.com/l/484441/2018-10-24/2hpgbx' + '?' + uriEncodedParams;
		console.log(targetUrl);
		

		$.ajax({
		    url: targetUrl,
		    dataType: 'jsonp',
		    jsonpCallback: 'callback',
		    success: function (data) {
			    console.log(data);
				$(".investigators_form form").submit();
				$(".hidden-submit").click();
			}
		});
		
		window.callback = function (data) {
			console.log(data);
			$(".investigators_form form").submit();
			$(".hidden-submit").click();
		}

		
	});
*/
	
	
	$("body").on("keyup",".investigators_form .company_name input[type='text']",function(e) {companyName = $(this).val();});
	$("body").on("keyup",".investigators_form .director_name input[type='text']",function(e) {directorName = $(this).val(); directorNamesplit = directorName.split(" ");});
	$("body").on("keyup",".investigators_form .director_email input[type='email']",function(e) {directorEmail = $(this).val();});
	$("body").on("keyup",".investigators_form .director_fax input[type='tel']",function(e) {directorFax = $(this).val();});
	$("body").on("keyup",".investigators_form .director_phone input[type='tel']",function(e) {directorPhone = $(this).val();});
	$("body").on("keyup",".investigators_form .areas textarea",function(e) {areasInput = $(this).val();});
	
	$( document ).on( 'nfFormSubmitResponse', function() {
    	var data = { 'company_name': companyName, 'director_fn': directorNamesplit[0], 'director_ln': directorNamesplit[1], 'email': directorEmail, 'fax': directorFax, 'phone': directorPhone, 'areas': areasInput };
		var uriEncodedParams = $.param(data);
		var targetUrl = 'http://go.celerion.com/l/484441/2018-10-24/2hpgbx' + '?' + uriEncodedParams;
		

		$.ajax({
		    url: targetUrl,
		    dataType: 'jsonp',
		    jsonpCallback: 'callback'
		});
		
		window.callback = function (data) {}
		
    });
	
	//NEWS SEARCH END
	$( ".accordion_trigger" ).click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(this).siblings(".holder.hidden").slideToggle( "fast", function() {
			$(this).siblings(".holder.hidden").removeClass("hidden");
		});
	});
	$( ".slide_down" ).click(function(e) {
		e.preventDefault();
		$([document.documentElement, document.body]).animate({
	        scrollTop: $("#bump_down").offset().top
	    }, 500);
	});
	function toggleSearch(){
		elem = $("header #main_nav .search");
		elem.toggleClass("active");
		if (elem.hasClass("active")){
			$(".search_form_holder").animate({
				width: "63%"
			}, 150, function() {
			});
			$("header #main_nav #menu-main-nav").animate({
				opacity: 0
			}, 150, function() {
			});
		} else {
			$(".search_form_holder").animate({
				width: "0%"
			}, 150, function() {
			});
			$("header #main_nav #menu-main-nav").animate({
				opacity: 1
			}, 150, function() {
			});
		}
	}
	$( window ).resize(function() {
	  if ($(window).width() >= 968) {
		  $("#nav_holder").attr("style","");
	  }
	  stickyMenu();
	});
	if ($("body#page_contact").length){
		$(".entry-content-page .pardotform").attr("height","800px");
	}
	stickyMenu();
	function stickyMenu(){
		headerHeight = $("header").outerHeight();
		$("header").attr("data-height",headerHeight);
		$(".trigger").css("top",headerHeight);
	};
	currentPos = 0;
	unstick = true;
	$(window).scroll(function(){
		var posTop = $(window).scrollTop() - $('.trigger').offset().top;
		if (posTop < currentPos && posTop > $("header").data("height")){
			if (!$("header").hasClass("sticky")){
				unstick = true;
				$("header").addClass("sticky").css("top","-150px");
				$("header").animate({
					top: "0"
				}, 150, function() {
				});
				$(".entry-content-page").addClass("sticky").css("margin-top",$("header").data("height"));
			}
			currentPos = posTop;
		} else {
			if ($("header").hasClass("sticky") && unstick == true){
				$("header").animate({
					top: "-"+$("header").data("height")
				}, 150, function() {
					$("header").css("top",0).removeClass("sticky");
					$(".entry-content-page").css("margin-top",0).removeClass("sticky");
				});
				unstick = false;
			}
			currentPos = posTop;
		}
	});
	
	//ISOTOPE
	$('.resource_holder .resources').isotope({
		itemSelector: '.item',
		percentPosition: true
	});
	$('.news_holder .resources').isotope({
		itemSelector: '.item',
		percentPosition: true,
		layoutMode: 'fitRows'
	});
	//Form Handlers
	
	if ($("#page_contact")[0]){
		
	// Create browser compatible event handler.
	  var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	  var eventer = window[eventMethod];
	  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	  // Listen for a message from the iframe.
	  eventer(messageEvent, function(e) {
	    if (isNaN(e.data)) return;
	 
	    // replace #sizetracker with what ever what ever iframe id you need
	    document.getElementById('sizetracker').style.height = e.data + 'px';
	 
	  }, false);
		
		
/*
		setTimeout(function(){
			$("#nf-form-3-cont form").attr("id","contact");
		}, 500);
		$("body").on("click","form#contact .submit_button",function(event) {
			event.preventDefault();
			var data = { 'fname' : $(this).find("input[name='fname']").val(), 'lname' : $(this).find("input[name='lname']").val(), 'phone' : $(this).find("input[name='phone']").val(), 'email' : $(this).find("input[name='email']").val(), 'nf-field-24' : $(this).find("input[name='nf-field-24']").val(), 'nf-field-29' : $(this).find("input[name='nf-field-29']").val() };
			var uriEncodedParams = $.param(data);
			var targetUrl = 'http://go.celerion.com/l/484441/2018-10-11/2hjy9n' + '?' + uriEncodedParams;
				
			console.log(targetUrl);
			$.ajax({
			    url: targetUrl,
			    dataType: 'jsonp',
			    jsonpCallback: 'callback'
			});
				
			window.callback = function (data) {
				console.log(data);
			}
		});
*/
		
/*
		setTimeout(function(){
			$("#nf-form-3-cont form").attr("method","post").attr("action","http://go.celerion.com/l/484441/2018-10-11/2hjy9n");
		}, 500);
*/
	}
});