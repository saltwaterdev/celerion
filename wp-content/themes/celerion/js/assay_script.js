jQuery(document).ready(function($) {
	$("#validated").change(function() {
	    if(this.checked) {
	    	$(".sm-row-content.toggle").show();
	    } else {
		    $(".sm-row-content.toggle").hide();
	    }
	});
	$( "#narrow" ).keyup(function() {
		$("#redirect").children("option").show();
		for(x=0;x<$("#redirect").children("option").length;x++){
			textVal = $( "#narrow" ).val().toLowerCase();
			if ($("#redirect").children("option").eq(x).text().toLowerCase().includes(textVal)){
				$("#redirect").children("option").eq(x).show();
			} else {
				$("#redirect").children("option").eq(x).hide();
			}
		}
	});
});