$( document ).ready(function() {
	
	if ($(".fl-module-hero-module")[0]){
		
		var controller = new ScrollMagic.Controller();
		
		var herobg = TweenMax.to('.fl-module-hero-module', 1, {
			backgroundPositionY: -100
		});
		
		var herobg_scene = new ScrollMagic.Scene({
			triggerElement: '.fl-module-hero-module',
		 	triggerHook: "onCenter",
		 	offset: 300,
			duration: 500 // pin the element for a total of 400px
		})
		.setTween(herobg)
		.addIndicators()
		.addTo(controller);
		
	}
	
});