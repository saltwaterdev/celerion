<?php get_header(); ?>
	
	<div class="container post event">
		<?php
			$eventinfo = get_post_meta( $post->ID );
	    while ( have_posts() ) : the_post(); ?>
	    	<?php if ( has_post_thumbnail() ) { 
		         echo '<div class="img" style="background-image: url('.get_the_post_thumbnail_url(get_the_ID()).')"></div>';
		        } ?>
	        <div class="entry-content-page post">
		        <h1><?php echo the_title(); ?></h1>
		        <?php 
			        if ( isset ( $eventinfo['conference_start'] ) ){
			        	$startdatearray = explode("-", $eventinfo['conference_start'][0]);
			        }
			        if ( isset ( $eventinfo['conference_end'] ) ){
			        	$enddatearray = explode("-", $eventinfo['conference_end'][0]);
			        }
		        ?>
		        <p class="byline date"> <?php if ( isset ( $eventinfo['conference_start'] ) ) echo $startdatearray[1].'.'.$startdatearray[2].'.'.$startdatearray[0]; ?> <?php if ( isset ( $eventinfo['conference_end'] ) ) echo ' - '. $enddatearray[1].'.'.$enddatearray[2].'.'.$enddatearray[0]; ?> <!-- - By John Smith --></p>
		        <p class="byline location"><?php if ( isset ( $eventinfo['conference_location'] ) ) echo $eventinfo['conference_location'][0]; ?></p>
		        <?php if ( has_post_thumbnail() ) { 
		         echo '<div class="img" style="background-image: url('.get_the_post_thumbnail_url(get_the_ID()).')"></div>';
		        } ?>
	            <?php if ( isset ( $eventinfo['conference_weblink'] ) && $eventinfo['conference_weblink'][0] !== "" ){ ?>
	            	<a href="<?php echo $eventinfo['conference_weblink'][0]; ?>" class="button black" target="_blank">Visit the Site</a>
	            <?php } ?>
	            <?php if ( isset ( $eventinfo['conference_email'] ) && $eventinfo['conference_email'][0] !== "" ){ ?>
	            	<a href="mailto:<?php echo $eventinfo['conference_email'][0]; ?>?subject=<?php echo the_title(); ?>" class="button black">Contact Us</a>
	            <?php } ?>
	            <?php the_content(); ?>
	            <div class="info">
		            <div class="sharethis-inline-share-buttons"></div>
	            </div>
	        </div>
	
	    <?php
	    endwhile;
	    wp_reset_query();
	    ?>
	</div>
<?php  get_footer(); ?>