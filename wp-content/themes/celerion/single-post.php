<?php get_header(); ?>
	
	<div class="container post">
		<?php
	    while ( have_posts() ) : the_post(); ?>
	        <div class="entry-content-page post">
		        <h1><?php echo the_title(); ?></h1>
		        <p class="byline"> <?php the_date('m.d.y'); ?> <!-- - By John Smith --></p>
		        <?php if ( get_the_post_thumbnail_url(get_the_ID()) !== "" ) { 
		        //	echo '<div class="img" style="background-image: url('.get_the_post_thumbnail_url(get_the_ID()).')"></div>';
		        } ?>
	            <?php the_content(); ?>
	            <div class="info">
		            <div class="sharethis-inline-share-buttons"></div>
	            </div>
	        </div>
	
	    <?php
	    endwhile;
	    wp_reset_query();
	    ?>
	</div>
<?php  get_footer(); ?>