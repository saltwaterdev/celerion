<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<?php
	if (!isset($_GET['s'])) {
		if (get_post_type(get_the_id()) == "resource"){
			$resourceinfo = get_post_meta(get_the_id());
			if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
				$resourcelink = $resourceinfo['resource_file'][0];
				header( "Location: $resourcelink" );
			}
		}
	}
?>

<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TD9FMX7');</script>
	<!-- End Google Tag Manager -->
  	<meta charset="utf-8">

	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
	
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css">
	
	<script type="text/javascript">
	var MTIProjectId='fcb7d8d4-5266-40ab-9d88-4e870623a785';
	 (function() {
	        var mtiTracking = document.createElement('script');
	        mtiTracking.type='text/javascript';
	        mtiTracking.async='true';
	         mtiTracking.src='/wp-content/themes/celerion/js/mtiFontTrackingCode.js';
	        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild( mtiTracking );
	   })();
	</script>
	
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-TXfwrfuHVznxCssTxWoPZjhcss/hp38gEOH8UPZG/JcXonvBQ6SlsIF49wUzsGno" crossorigin="anonymous">
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.css">
	
	<?php wp_head(); ?>
	
	<?php
		global $post;
		if (!empty($post)){
			$post_slug=$post->post_name;
		}
    ?>
	
	<!--[if lt IE 9]>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
	
	<script type='text/javascript'>
	(function (d, t) {
	  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	  bh.type = 'text/javascript';
	  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=mzrautaajvgazbzhdb1hjq';
	  s.parentNode.insertBefore(bh, s);
	  })(document, 'script');
	</script>
	
	<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5b992da8c265ef0011d17468&product=custom-share-buttons"></script>
	
	<script src='<?php echo get_template_directory_uri(); ?>/js/jqueryCookieGuard.1.0.min.js'></script>
	<script>
	  jQuery(document).ready(function($) {
		if(typeof CCM_SECURITY_TOKEN === 'undefined'){
			$.cookieguard();
			$.cookieguard.cookies.add('__sharethis_cookie_test__', '__sharethis_cookie_test__', 'This cookie is part of the ShareThis service and allows you to share specific content to various social networking applications.', true);
			$.cookieguard.cookies.add('visitor_id', 'visitor_id', 'This is your unique identifier. This is used for tracking purposes and to remember preferences through the website.', true);
			$.cookieguard.cookies.add('_ga', '_ga', 'Registers a unique ID to generate statistical data.', false);
			$.cookieguard.cookies.add('_gid', '_gid', 'Used to distinguish users.', false);
			$.cookieguard.cookies.add('_gat_UA-45904858-1', '_gat_UA-45904858-1', 'Used to throttle analytics request rate.', false);
			$.cookieguard.cookies.add('viewed_cookie_policy', 'viewed_cookie_policy', 'Confirms you have viewed the cookie policy.', false);
			$.cookieguard.run();
		}
	  });
	</script>
	
</head>

<body id="page_<?php echo $post_slug; ?>">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TD9FMX7"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
	<header>
		<div class="container">
			<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				if ( has_custom_logo() ) {
				        echo '<a href="/" class="logo"><img src="'. esc_url( $logo[0] ) .'"></a>';
				}
			?>
			<a href="#" class="menu_open"><i class="fas fa-bars"></i></a>
			<?php include 'elements/navigation.php'; ?>
		</div>
	</header>
	<div class="trigger"></div>