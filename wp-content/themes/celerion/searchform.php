<div class="search_form_holder">
<form action="/" method="get" class="search_form">
    <input type="text" name="s" id="search" placeholder="What can we help you find?" value="<?php the_search_query(); ?>" />
    <button type="submit" id="submit"><i class="fas fa-search"></i></button>
    <a href="#" class="close"><i class="far fa-times"></i></a>
</form>
</div>