<?php
	
	add_theme_support( 'custom-logo' );
	
	register_nav_menus(
		array(
			'main_nav' => __('Main Nav','celerion'),
			'utility_nav' => __('Utility Nav','celerion'),
			'footer_nav' => __('Footer Nav','celerion'),
		)	
	);
	
	function celerion_widgets() {
		register_sidebar( array(
			'name'          => 'Footer Column One',
			'id'            => 'footer_col_one',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
		));
		register_sidebar( array(
			'name'          => 'Footer Column Two',
			'id'            => 'footer_col_two',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
		));
		register_sidebar( array(
			'name'          => 'Footer Column Three',
			'id'            => 'footer_col_three',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
		));
		register_sidebar( array(
			'name'          => 'Footer Column Four',
			'id'            => 'footer_col_four',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
		));
	}
	add_action( 'widgets_init', 'celerion_widgets' );
	
	function celerion_mce_buttons($buttons) {
	    array_unshift($buttons, 'styleselect');
	    return $buttons;
	}
	add_filter('mce_buttons_2', 'celerion_mce_buttons');
	
	/*
	* Callback function to filter the MCE settings
	*/
	 
	function celerion_mce_before_init_insert_formats( $init_array ) {  
	 
	// Define the style_formats array
	 
	    $style_formats = array(  
	/*
	* Each array child is a format with it's own settings
	* Notice that each array has title, block, classes, and wrapper arguments
	* Title is the label which will be visible in Formats menu
	* Block defines whether it is a span, div, selector, or inline style
	* Classes allows you to define CSS classes
	* Wrapper whether or not to add a new block-level element around any selected elements
	*/

	        array(  
	            'title' => 'Clear Button',  
	            'block' => 'span',  
	            'classes' => 'button',
	            'wrapper' => true,
	        ), 
	        array(  
	            'title' => 'White Button',  
	            'block' => 'span',  
	            'classes' => 'button white',
	            'wrapper' => true,
	        ),
	        array(  
	            'title' => 'Gray Button',  
	            'block' => 'span',  
	            'classes' => 'button gray',
	            'wrapper' => true,
	        ),
	        array(  
	            'title' => 'Link with Right Arrow',  
	            'block' => 'span',  
	            'classes' => 'arrow arrowr',
	            'wrapper' => true,
	        ),
	        array(  
	            'title' => 'Section Title',  
	            'block' => 'span',  
	            'classes' => 'section-title',
	            'wrapper' => true,
	        ),
	    );  
	    // Insert the array, JSON ENCODED, into 'style_formats'
	    $init_array['style_formats'] = json_encode( $style_formats );  
	     
	    return $init_array;  
	   
	} 
	// Attach callback to 'tiny_mce_before_init' 
	add_filter( 'tiny_mce_before_init', 'celerion_mce_before_init_insert_formats' ); 
	add_theme_support( 'post-thumbnails' );
	
	function create_post_type() {
		register_post_type( 'assay',
	    	array(
				'labels' => array(
					'name' => __( 'Assays' ),
					'singular_name' => __( 'Assay' )
				),
			'public' => true,
			'has_archive' => false,
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' => 'dashicons-laptop',
			'rewrite' => array('slug' => 'assay'),
			'taxonomies' => array('post_tag'),
			)
		);
		register_post_type( 'resource',
	    	array(
				'labels' => array(
					'name' => __( 'Resources' ),
					'singular_name' => __( 'Resource' )
				),
			'public' => true,
			'has_archive' => false,
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' => 'dashicons-laptop',
			'rewrite' => array('slug' => 'resource'),
			'taxonomies' => array('post_tag'),
			)
		);
		register_post_type( 'event',
	    	array(
				'labels' => array(
					'name' => __( 'Events' ),
					'singular_name' => __( 'Event' )
				),
			'public' => true,
			'has_archive' => false,
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'menu_icon' => 'dashicons-laptop',
			'rewrite' => array('slug' => 'event'),
			'taxonomies' => array('post_tag'),
			)
		);
	}
	add_action( 'init', 'create_post_type' );
	
	//START CUSTOM TAXONOMY
	function themes_taxonomy() {  
	    register_taxonomy(  
	        'type',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
	        'resource',        //post type name
	        array(  
	            'hierarchical' => true,  
	            'label' => 'Resource Type',  //Display name
	            'query_var' => true,
	            'rewrite' => array(
	                'slug' => 'type', // This controls the base slug that will display before each term
	                'with_front' => false // Don't display the category base before 
	            )
	        )  
	    );  
	}  
	add_action( 'init', 'themes_taxonomy');

	
	function prfx_script_enqueue() {
		global $post;
		wp_register_script( 'assay_scripts', get_template_directory_uri() . '/js/assay_script.js', array( 'jquery' ) );
		wp_enqueue_script( 'assay_scripts' );
	}
	add_action( 'admin_enqueue_scripts', 'prfx_script_enqueue' );
	
	function prfx_image_enqueue() {
		global $post;
	        wp_enqueue_media();
	 
	        // Registers and enqueues the required javascript.
	        wp_register_script( 'meta-box-image', get_template_directory_uri() . '/js/meta-box-image.js', array( 'jquery' ) );
	        wp_localize_script( 'meta-box-image', 'meta_image',
	            array(
	                'title' => __( 'Choose or Upload an Resource', 'resource_mediatitle' ),
	                'button' => __( 'Use this Resource', 'resource_mediatitle' ),
	            )
	        );
	        wp_enqueue_script( 'meta-box-image' );
	        wp_enqueue_script("jquery-ui",'//code.jquery.com/ui/1.11.4/jquery-ui.js', array('jquery'), '1.11.4');
	        wp_enqueue_script("jquery-ui");
	}
	add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );
	
	function prfx_admin_styles(){
	    global $post;
	    wp_enqueue_style( 'assay_styles', get_template_directory_uri() . '/css/assay_script.css' );
	}
	add_action( 'admin_print_styles', 'prfx_admin_styles' );
	
	function sm_custom_meta() {
	    add_meta_box( 'assay_meta', __( 'Assay Information', 'assay-domain' ), 'assay_callback', 'assay' );
	    add_meta_box( 'redirect_meta', __( 'Redirect To', 'redirect-domain' ), 'redirect_callback', 'page' );
	    add_meta_box( 'resource_meta', __( 'Resource Information', 'resource-domain' ), 'resource_callback', 'resource' );
	    add_meta_box( 'event_meta', __( 'Event Information', 'event-domain' ), 'event_callback', 'event' );
	    add_meta_box( 'posts_meta', __( 'News Information', 'posts-domain' ), 'posts_callback', 'post' );
	}
	
	add_action( 'add_meta_boxes', 'sm_custom_meta' );
	
	function posts_callback( $post ) { 
		$postinfo = get_post_meta( $post->ID );
	?>
		<h3>If not an internal news story, choose one of the following methods of news:</h3>
		<p>
			<div class="sm-row-content">
				<label for="external_link"><b>External Link:</b></label>
				<input type="text" id="external_link" name="external_link" value="<?php if ( isset ( $postinfo['external_link'] ) ) echo $postinfo['external_link'][0]; ?>" / >
			</div>
		</p>
		<hr>
		<p>
			<label for="post_file"><b>News File:</b></label>
			<input type="text" name="resource_file" style="display: none;" id="post_file" value="<?php if ( isset ( $postinfo['resource_file'] ) ) echo $postinfo['resource_file'][0]; ?>" class="picval"/>
			<a href="<?php if ( isset ( $postinfo['resource_file'] ) ){ echo $postinfo['resource_file'][0]; } ?>" class="post_link" style="display: block;" target="_blank"><?php if ( isset ( $postinfo['resource_file'] ) ){ echo $postinfo['resource_file'][0]; } ?></a>
			<input type="button" class="picbtn button" id="post_file-button" class="button picbtn" value="<?php _e( 'Choose a News File', 'myplugin_textdomain' )?>" />
			<input type="button" class="removepic button" id="removepic" value="Remove News File">
</p>
	<?php }
	
	function resource_callback( $post ) { 
		$resourceinfo = get_post_meta( $post->ID );
	?>
		<h3>Choose one of the following methods of resource:</h3>
		<p>
			<div class="sm-row-content">
				<label for="external_link"><b>External Link:</b></label>
				<input type="text" id="external_link" name="external_link" value="<?php if ( isset ( $resourceinfo['external_link'] ) ) echo $resourceinfo['external_link'][0]; ?>" / >
			</div>
		</p>
		<hr>
		<p>
			<label for="resource_file"><b>Resource File:</b></label>
			<input type="text" name="resource_file" style="display: none;" id="resource_file" value="<?php if ( isset ( $resourceinfo['resource_file'] ) ) echo $resourceinfo['resource_file'][0]; ?>" class="picval"/>
			<?php
				if ( isset ( $resourceinfo['resource_file'] ) ){
					$resourcelink = $resourceinfo['resource_file'][0];
					if (strpos($resourcelink, 'celerion.com') == false) {
					    $resourcelink = get_permalink($resourcelink);
					}
				}
			?>
			<a href="<?php if ( isset ( $resourceinfo['resource_file'] ) ){ echo $resourcelink; } ?>" class="resource_link" style="display: block;" target="_blank"><?php if ( isset ( $resourceinfo['resource_file'] ) ){ echo $resourcelink; } ?></a>
			<input type="button" class="picbtn button" id="resource_file-button" class="button picbtn" value="<?php _e( 'Choose a Resource File', 'myplugin_textdomain' )?>" />
			<input type="button" class="removepic button" id="removepic" value="Remove Resource File">
</p>
	<?php }
	
	function event_callback( $post ) { 
		$eventinfo = get_post_meta( $post->ID );
	?>
		<p>
			<div class="sm-row-content">
				<label for="conference_start">Start Date:</label>
				<?php if ( isset ( $eventinfo['conference_start'] )){
					$startdate = $eventinfo['conference_start'][0];
				} else {
					$startdate = date("Y-m-d");
				}
				if ( isset ( $eventinfo['conference_end'] )){
					$enddate = $eventinfo['conference_end'][0];
				} else {
					$enddate = date("Y-m-d");
				}
				?>
				<input type="date" id="conference_start" name="conference_start" value="<?php echo $startdate; ?>" min="2000-01-01" max="2099-12-31"/ >
			</div>
			<div class="sm-row-content">
				<label for="conference_end">End Date:</label>
				<input type="date" id="conference_end" name="conference_end" value="<?php echo $enddate; ?>" min="2000-01-01" max="2099-12-31"/ >
			</div>
			<div class="sm-row-content">
				<label for="conference_location">Location</label>
				<input type="text" id="conference_location" name="conference_location" value="<?php if ( isset ( $eventinfo['conference_location'] ) ) echo $eventinfo['conference_location'][0]; ?>" / >
			</div>
			<div class="sm-row-content">
				<label for="conference_weblink">Link for Event:</label>
				<input type="text" id="conference_weblink" name="conference_weblink" value="<?php if ( isset ( $eventinfo['conference_weblink'] ) ) echo $eventinfo['conference_weblink'][0]; ?>" / >
			</div>
			<div class="sm-row-content">
				<label for="conference_email">Email:</label>
				<input type="text" id="conference_email" name="conference_email" value="<?php if ( isset ( $eventinfo['conference_email'] ) ) echo $eventinfo['conference_email'][0]; ?>" / >
			</div>
		</p>
	<?php }
	
	function redirect_callback( $post ) { 
		$redirectinfo = get_post_meta( $post->ID );
	?>
		<p>
			<div class="sm-row-content">
				<label for="redirect">Redirect To:</label>
				<input type="text" id="narrow" placeholder="Type to narrow potential pages">
				<select size="3" style="height: 200px;" name="redirect" id="redirect">
					<option selected="selected" value="0"><?php echo esc_attr( __( 'None' ) ); ?></option>
					<?php
				        $selected_page = get_option( 'option_key' );
				        $pages = get_pages(); 
				        foreach ( $pages as $page ) {
				            $option = '<option value="' . $page->ID . '" ';
				            $option .= ( $page->ID == $redirectinfo['redirect'][0] ) ? 'selected="selected"' : '';
				            $option .= '>';
				            $option .= $page->post_title;
				            $option .= '</option>';
				            echo $option;
				        }
				    ?>
				</select>
			</div>
		</p>
	<?php }
	
	function assay_callback( $post ) {
    	$assayinfo = get_post_meta( $post->ID );
    	$technique_vals = array("LC-MS/MS","EIA","RIA","ELISA","Enzyme Assay","IRMA","HPLC/FL","ICP/MS");
    	$species_vals = array("Human Serum","Human Plasma","Human Urine","Human Saliva","Human Whole Blood","Human Red Blood Cells","Rat Plasma","Dog Plasma","Mouse Plasma","Rabbit Serum");
    	if ($assayinfo['technique']){
    		$selected_technique = $assayinfo['technique'][0];
    	}
    	if ($assayinfo['species-matrix']){
    		$selected_species = $assayinfo['species-matrix'][0];
    	}
    ?>
    	<p>
			<div class="sm-row-content">
				<label for="technique">Technique</label>
				<select name="technique" id="technique">
					<option value=""></option>
					<?php foreach($technique_vals as $tech_val){?>
						<option value="<?php echo $tech_val; ?>" <?php if($selected_technique){if ($tech_val == $selected_technique){echo 'selected="selected"';} }?>><?php echo $tech_val; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="sm-row-content">
				<label for="species-matrix">Species/Matrix</label>
		        <select name="species-matrix" id="species-matrix">
			        <option value=""></option>
					<?php foreach($species_vals as $species_val){?>
						<option value="<?php echo $species_val; ?>" <?php if($selected_species){if ($species_val == $selected_species){echo 'selected="selected"';}} ?>><?php echo $species_val; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="sm-row-content">
				<label for="validated">Validated</label>
		        <input type="checkbox" name="validated" id="validated" value="1" <?php if ( isset ( $assayinfo['validated'] ) ) checked( $assayinfo['validated'][0], '1' ); ?> />
			</div>
			<div class="sm-row-content">
				<label for="biomarkerassays">Biomarker Assays</label>
		        <input type="checkbox" name="biomarkerassays" id="biomarkerassays" value="1" <?php if ( isset ( $assayinfo['biomarkerassays'] ) ) checked( $assayinfo['biomarkerassays'][0], '1' ); ?> />
			</div>
			<div class="sm-row-content">
				<label for="pkassays">Biosimilar Qualified Assays – PK Assays</label>
		        <input type="checkbox" name="pkassays" id="pkassays" value="1" <?php if ( isset ( $assayinfo['pkassays'] ) ) checked( $assayinfo['pkassays'][0], '1' ); ?> />
			</div>
			<div class="sm-row-content">
				<label for="adaassays">Biosimilar Qualified Assays – ADA Assays</label>
		        <input type="checkbox" name="adaassays" id="adaassays" value="1" <?php if ( isset ( $assayinfo['adaassays'] ) ) checked( $assayinfo['adaassays'][0], '1' ); ?> />
			</div>
			<div class="sm-row-content toggle" <?php if ($assayinfo['validated'][0] == 1 || $assayinfo['biomarkerassays'][0] == 1 || $assayinfo['pkassays'][0] == 1 || $assayinfo['adaassays'][0] == 1){echo 'style="display: block;"';}?>>
				<label for="range">Range</label>
		        <input type="text" name="range" id="range" value="<?php if ( isset ( $assayinfo['range'] ) ) echo $assayinfo['range'][0]; ?>"/>
			</div>
			<div class="sm-row-content toggle" <?php if ($assayinfo['validated'][0] == 1 || $assayinfo['biomarkerassays'][0] == 1 || $assayinfo['pkassays'][0] == 1 || $assayinfo['adaassays'][0] == 1){echo 'style="display: block;"';}?>>
				<label for="anti-coag">Anti-coag.</label>
		        <input type="text" name="anti-coag" id="anti-coag" value="<?php if ( isset ( $assayinfo['anti-coag'] ) ) echo $assayinfo['anti-coag'][0]; ?>"/>
			</div>
			<div class="sm-row-content">
				<label for="keywords">Keywords</label>
				<textarea id="keywords" name="keywords"><?php if ( isset ( $assayinfo['keywords'] ) ) echo $assayinfo['keywords'][0]; ?></textarea>
			</div>
    	</p>
    <?php }
	    
	function assay_shortcode($atts) {
		
		$a = shortcode_atts( array(
	       'url' => ''
		), $atts );
		
		$content = '';
		$content .= '<form class="assay_search"><h4>Use the search feature below to browse our full list of assays—the most comprehensive list in the industry.</h4><input type="text" placeholder="Search Assays" class="search_box"><button type="submit" class="submit">Search<i class="fas fa-search"></i></button><div class="filter"><label for="filterby">Filter by:</label><select name="filterby" id="filterby"><option value="1">All</option>';
		$technique_vals = array("LC-MS/MS","EIA","RIA","ELISA","Enzyme Assay","IRMA","HPLC/FL","ICP/MS");
		foreach ($technique_vals as $technique){
			$content .= '<option value="'.$technique.'">'.$technique.'</option>';
		}
		$content .= '</select></div></form>';
		
		
		$content .= '<h3><span class="section-title">Assays Under Development</span></h3>';
		$content .= '<div class="tableholder"><table class="assay searchable"><thead><tr><th width="63%">Compound Name</th><th width="18%">Technique</th><th width="19%">Species/Matrix</th></tr></thead><tbody>';
		
		$args = array(
		    'posts_per_page' => -1,
			'post_type' => "assay",
			'orderby'=> 'title', 
			'order' => 'ASC',
			'post_status'      => 'publish',
		);
		$assay_list = get_posts( $args );
		
		foreach($assay_list as $assay){
			$assayinfo = get_post_meta( $assay->ID );
			if ($assayinfo['validated'][0] == 0){
				if ($assayinfo['technique']){$technique = $assayinfo['technique'][0];} else {$technique = "N/A";}
				if ($assayinfo['species-matrix']){$species = $assayinfo['species-matrix'][0];} else {$species = "N/A";}
				$content .= '<tr><td>'.get_the_title($assay->ID).'</td><td>'.$technique.'</td><td>'.$species.'</td></tr>';
			}
		}
		
		$content .= '</tbody></table></div>';
		
		//Biomarkers
		$content .= '<h3 id="biomarker"><span class="section-title">Biomarker Assays</span></h3>';
		$content .= '<div class="tableholder"><table class="assay searchable"><thead><tr><th width="40%">Compound Name</th><th width="12%">Technique</th><th width="13%">Species/Matrix</th><th width="23%">Range</th><th width="12%">Anti-coag.</th></tr></thead><tbody>';
		
		foreach($assay_list as $assay){
			$assayinfo = get_post_meta( $assay->ID );
			if ($assayinfo['biomarkerassays'][0] == 1){
				if ($assayinfo['technique']){$technique = $assayinfo['technique'][0];} else {$technique = "N/A";}
				if ($assayinfo['species-matrix']){$species = $assayinfo['species-matrix'][0];} else {$species = "N/A";}
				if ($assayinfo['range']){$range = $assayinfo['range'][0];} else {$range = "N/A";}
				if ($assayinfo['anti-coag']){$anticoag = $assayinfo['anti-coag'][0];} else {$anticoag = "N/A";}
				$content .= '<tr><td>'.get_the_title($assay->ID).'</td><td>'.$technique.'</td><td>'.$species.'</td><td>'.$range.'</td><td>'.$anticoag.'</td></tr>';
			}
		}
		
		$content .= '</tbody></table></div>';
		
		//PK Assays
		$content .= '<h3 id="pkassays"><span class="section-title">Biosimilar Qualified Assays - PK Assays</span></h3>';
		$content .= '<div class="tableholder"><table class="assay searchable"><thead><tr><th width="40%">Compound Name</th><th width="12%">Technique</th><th width="13%">Species/Matrix</th><th width="23%">Range</th><th width="12%">Anti-coag.</th></tr></thead><tbody>';
		
		foreach($assay_list as $assay){
			$assayinfo = get_post_meta( $assay->ID );
			if ($assayinfo['pkassays'][0] == 1){
				if ($assayinfo['technique']){$technique = $assayinfo['technique'][0];} else {$technique = "N/A";}
				if ($assayinfo['species-matrix']){$species = $assayinfo['species-matrix'][0];} else {$species = "N/A";}
				if ($assayinfo['range']){$range = $assayinfo['range'][0];} else {$range = "N/A";}
				if ($assayinfo['anti-coag']){$anticoag = $assayinfo['anti-coag'][0];} else {$anticoag = "N/A";}
				$content .= '<tr><td>'.get_the_title($assay->ID).'</td><td>'.$technique.'</td><td>'.$species.'</td><td>'.$range.'</td><td>'.$anticoag.'</td></tr>';
			}
		}
		
		$content .= '</tbody></table></div>';
		
		//ADA Assays
		$content .= '<h3 id="adaassays"><span class="section-title">Biosimilar Qualified Assays - ADA Assays</span></h3>';
		$content .= '<div class="tableholder"><table class="assay searchable"><thead><tr><th width="40%">Compound Name</th><th width="12%">Technique</th><th width="13%">Species/Matrix</th><th width="23%">Range</th><th width="12%">Anti-coag.</th></tr></thead><tbody>';
		
		foreach($assay_list as $assay){
			$assayinfo = get_post_meta( $assay->ID );
			if ($assayinfo['adaassays'][0] == 1){
				if ($assayinfo['technique']){$technique = $assayinfo['technique'][0];} else {$technique = "N/A";}
				if ($assayinfo['species-matrix']){$species = $assayinfo['species-matrix'][0];} else {$species = "N/A";}
				if ($assayinfo['range']){$range = $assayinfo['range'][0];} else {$range = "N/A";}
				if ($assayinfo['anti-coag']){$anticoag = $assayinfo['anti-coag'][0];} else {$anticoag = "N/A";}
				$content .= '<tr><td>'.get_the_title($assay->ID).'</td><td>'.$technique.'</td><td>'.$species.'</td><td>'.$range.'</td><td>'.$anticoag.'</td></tr>';
			}
		}
		
		$content .= '</tbody></table></div>';
		
		//VALIDATED
		$content .= '<h3 id="validated"><span class="section-title">Validated Assay List</span></h3>';
		$content .= '<div class="tableholder"><table class="assay searchable"><thead><tr><th width="40%">Compound Name</th><th width="12%">Technique</th><th width="13%">Species/Matrix</th><th width="23%">Range</th><th width="12%">Anti-coag.</th></tr></thead><tbody>';
		
		foreach($assay_list as $assay){
			$assayinfo = get_post_meta( $assay->ID );
			if ($assayinfo['validated'][0] == 1){
				if ($assayinfo['technique']){$technique = $assayinfo['technique'][0];} else {$technique = "N/A";}
				if ($assayinfo['species-matrix']){$species = $assayinfo['species-matrix'][0];} else {$species = "N/A";}
				if ($assayinfo['range']){$range = $assayinfo['range'][0];} else {$range = "N/A";}
				if ($assayinfo['anti-coag']){$anticoag = $assayinfo['anti-coag'][0];} else {$anticoag = "N/A";}
				$content .= '<tr><td>'.get_the_title($assay->ID).'</td><td>'.$technique.'</td><td>'.$species.'</td><td>'.$range.'</td><td>'.$anticoag.'</td></tr>';
			}
		}
		
		$content .= '</tbody></table></div>';
		return $content;
	
	}
	
	add_shortcode('assays', 'assay_shortcode');
	
	function resources_shortcode($atts) {
		$content = '';
		$content .= '<form class="resource_search"><h4><span class="section-title">Use the search feature below to browse our latest resources on research and innovation in clinical development.</span></h4><input type="text" placeholder="Search Resources" class="search_box"><button type="submit" class="submit">Search<i class="fas fa-chevron-right"></i></button><div class="filter"><label for="filterby">Filter by:</label><select name="filterby" id="filterby"><option value="1">Type</option>';
		$terms = get_terms( array(
		    'taxonomy' => 'type',
		    'hide_empty' => false,
		));
		foreach ($terms as $term){
			$content .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
		}
		$content .= '</select></div></form>';
		$content .= '<div class="resource_holder"><div class="resources">';
		
		$args = array(
		    'posts_per_page' => -1,
			'post_type' => "resource",
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_status'      => 'publish',
		);
		$resource_list = get_posts( $args );
		$count = 0;
		foreach($resource_list as $resource){
			$show = "activeitem";
			if ($count>=8){
 				$show = "hide";
			} 
			$terms = get_the_terms($resource->ID, 'type');
			$resourceinfo = get_post_meta($resource->ID);
			$resourcelink = get_permalink($resource->ID);
			$target = "";
			if ($resourceinfo['resource_file'][0] && $resourceinfo['resource_file'][0] !== ""){
				$resourcelink = $resourceinfo['resource_file'][0];
				if (strpos($resourcelink, 'celerion.com') == false) {
				    $resourcelink = get_permalink($resourcelink);
				}
				$target = 'target="_blank"';
			}
			if ($resourceinfo['external_link'][0] && $resourceinfo['external_link'][0] !== ""){
				$resourcelink = $resourceinfo['external_link'][0];
				$target = 'target="_blank"';
			}
			$thumbnail = "";
			if (get_the_post_thumbnail_url($resource->ID)){
				if ( $terms && ! is_wp_error( $terms ) ){
					foreach ( $terms as $term ) {
						if ($term->slug == "articles"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/article.png); background-size: 55%;"';
						} else if ($term->slug == "case-study"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/case_study.png); background-size: 55%;"';
						} else if ($term->slug == "presentation"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/presentation.png); background-size: 55%;"';
						} else if ($term->slug == "recorded-webex"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
						} else if ($term->slug == "scientific-poster"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/scientific_poster.png); background-size: 55%;"';
						} else if ($term->slug == "video"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/video.png); background-size: 55%;"';
	    				} else if ($term->slug == "webinar"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
	    				} else if ($term->slug == "white-papers"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/whitepaper.png); background-size: 55%;"';
	    				} else if ($term->slug == "fact-sheet"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/fact-sheet-icon.png); background-size: 55%;"';
	    				} else{
		    				$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
	    				}
					}
				} else {
					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
				}
// 				$thumbnail = 'style="background-image: url('.get_the_post_thumbnail_url($resource->ID).')"';
			} else {
				if ( $terms && ! is_wp_error( $terms ) ){
					foreach ( $terms as $term ) {
						if ($term->slug == "articles"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/article.png); background-size: 55%;"';
						} else if ($term->slug == "case-study"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/case_study.png); background-size: 55%;"';
						} else if ($term->slug == "presentation"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/presentation.png); background-size: 55%;"';
						} else if ($term->slug == "recorded-webex"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
						} else if ($term->slug == "scientific-poster"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/scientific_poster.png); background-size: 55%;"';
						} else if ($term->slug == "video"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/video.png); background-size: 55%;"';
	    				} else if ($term->slug == "webinar"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/webinar_recorded_webex.png); background-size: 55%;"';
	    				} else if ($term->slug == "white-papers"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/whitepaper.png); background-size: 55%;"';
	    				} else if ($term->slug == "fact-sheet"){
	    					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/icons/fact-sheet-icon.png); background-size: 55%;"';
	    				} else{
		    				$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
	    				}
					}
				} else {
					$thumbnail = 'style="background-image: url(/wp-content/themes/celerion/img/celerion_mark.png); background-size: contain;"';
				}
			}
			$content .= '<div class="item '.$show.'"><div class="img" '.$thumbnail.'></div><div class="content"><a href="'.$resourcelink.'" class="title" '.$target.'>'.get_the_title($resource->ID).'</a><p class="by">';
			if ( $terms && ! is_wp_error( $terms ) ){
				foreach ( $terms as $term ) {
					$content .= '<span class="category '.$term->slug.'">'.$term->name.'</span>';
				}
			}
			$content .= '</p></div></div>';
			$count++;
		}
		$content .= '</div>';
		$content .= '<a href="#" class="more">Load More <i class="fal fa-chevron-circle-down"></i></a>';
		$content .= '</div>';
		
		return $content;
	}
	
	add_shortcode('resources', 'resources_shortcode');
	
	function news_shortcode($atts) {
		$content = '';
		$content .= '<form class="news_search"><h4>Use the search feature below to stay up to date on our latest news stories.</h4><input type="text" placeholder="Search News & Events" class="search_box"><button type="submit" class="submit">Search<i class="fas fa-chevron-right"></i></button><div class="filter"><label for="filterby">Filter by:</label><select name="filterby_type" id="filterby"><option value="1">All Tags</option>';
		$tags = get_tags(array(
		  'hide_empty' => false
		));
		foreach ($tags as $tag) {
			$content .= '<option value="'.$tag->slug.'">'.ucwords($tag->name).'</option>';
		}
		$content .= '</select>';
		$terms = get_terms( array(
		    'taxonomy' => 'category',
		    'hide_empty' => false,
		));
		foreach ($terms as $term){
// 			$content .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
		}
		$content .= '</div></form>';
		$content .= '<div class="news_holder"><div class="resources">';
		
		$args = array(
		    'posts_per_page' => -1,
			'post_type' =>  array('post'),
			'category' => 32,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_status'      => 'publish',
		);
		$resource_list = get_posts( $args );
		$count = 0;
		foreach($resource_list as $resource){
			$show = "activeitem";
			if ($count>=8){
 				$show = "hide";
			}
			$resourceinfo = get_post_meta($resource->ID);
			$resourcelink = get_permalink($resource->ID);
			$target = "";
			if (isset($resourceinfo['resource_file']) && $resourceinfo['resource_file'][0] !== ""){
// 				$resourcelink = wp_get_attachment_url($resourceinfo['resource_file'][0]);
// 				$target = 'target="_blank"';
			}
			if (isset($resourceinfo['external_link']) && $resourceinfo['external_link'][0] !== ""){
				$resourcelink = $resourceinfo['external_link'][0];
				$target = 'target="_blank"';
			}
			$thumbnail = "";
			if (get_the_post_thumbnail_url($resource->ID)){
				$thumbnail = 'style="background-image: url('.get_the_post_thumbnail_url($resource->ID).')"';
			}
			$typenumber = "type_";
			if (get_post_type($resource->ID) == "event"){
				$typenumber .= "4";
			} else {
				$terms = get_the_terms($resource->ID, 'category');
				$check = false;
				if ( $terms && ! is_wp_error( $terms ) ){
					foreach ( $terms as $term ) {
						if ($term->name == "Blog"){
							$check = true;
						};
					}
				}
				if ($check == true){
					$typenumber .= "3";
				} else {
					$typenumber .= "2";
				}
			}
			$standby = "news_item sb";
			$content .= '<div class="item '.$standby.' '.$typenumber.' '.$show.'"><div class="img"></div><div class="content"><a href="'.$resourcelink.'" class="title" '.$target.'>'.get_the_title($resource->ID).'</a>';
			$content .= '<div class="hidden tags">';
			$post_tags = get_the_tags($resource->ID);
			if ( $post_tags ) {
				$tagstring = "";
			    foreach( $post_tags as $tag ) {
			    	$tagstring .= ' '.$tag->slug;
			    }
			    $content .= $tagstring;
			}
			$content .= '</div>';
			$content .= '<p class="by">';
			if (get_post_type($resource->ID) == "post"){
				$content .= '<span class="category">'.get_the_date('m.d.y',$resource->ID).'</span>';
			} else {
				if ( isset ( $resourceinfo['conference_start'] ) ){
			        $startdatearray = explode("-", $resourceinfo['conference_start'][0]);
			    }
			    if ( isset ( $resourceinfo['conference_end'] ) ){
			        $enddatearray = explode("-", $resourceinfo['conference_end'][0]);
			    }
				$content .= '<span class="category">'.$startdatearray[1].'.'.$startdatearray[2].'.'.$startdatearray[0].' - '.$resourceinfo['conference_location'][0].'</span>';
			}
			$content .= '</p></div></div>';
			$count++;
		}
 		$content .= '</div><a href="#" class="more">Load More <i class="fal fa-chevron-circle-down"></i></a>';
		$content .= '</div>';
		
		return $content;
	}
	
	add_shortcode('news_feed', 'news_shortcode');
	
	function blog_shortcode($atts) {
		$content = '';
		$content .= '<form class="news_search"><h4>Use the search feature below to stay up to date on our latest blog posts.</h4><input type="text" placeholder="Search News & Events" class="search_box"><button type="submit" class="submit">Search<i class="fas fa-chevron-right"></i></button><div class="filter"><label for="filterby">Filter by:</label><select name="filterby_type" id="filterby"><option value="1">All Tags</option>';
		$tags = get_tags(array(
		  'hide_empty' => false
		));
		foreach ($tags as $tag) {
			$content .= '<option value="'.$tag->slug.'">'.ucwords($tag->name).'</option>';
		}
		$content .= '</select>';
		$terms = get_terms( array(
		    'taxonomy' => 'category',
		    'hide_empty' => false,
		));
		foreach ($terms as $term){
// 			$content .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
		}
		$content .= '</div></form>';
		$content .= '<div class="news_holder"><div class="resources">';
		
		$args = array(
		    'posts_per_page' => -1,
			'post_type' =>  array('post'),
			'category' => 27,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_status'      => 'publish',
		);
		$resource_list = get_posts( $args );
		$count = 0;
		foreach($resource_list as $resource){
			$show = "activeitem";
			if ($count>=8){
 				$show = "hide";
			}
			$resourceinfo = get_post_meta($resource->ID);
			$resourcelink = get_permalink($resource->ID);
			$target = "";
			if (isset($resourceinfo['resource_file']) && $resourceinfo['resource_file'][0] !== ""){
				$resourcelink = wp_get_attachment_url($resourceinfo['resource_file'][0]);
				$target = 'target="_blank"';
			}
			if (isset($resourceinfo['external_link']) && $resourceinfo['external_link'][0] !== ""){
				$resourcelink = $resourceinfo['external_link'][0];
				$target = 'target="_blank"';
			}
			$thumbnail = "";
			if (get_the_post_thumbnail_url($resource->ID)){
				$thumbnail = 'style="background-image: url('.get_the_post_thumbnail_url($resource->ID).')"';
			}
			$typenumber = "type_";
			if (get_post_type($resource->ID) == "event"){
				$typenumber .= "4";
			} else {
				$terms = get_the_terms($resource->ID, 'category');
				$check = false;
				if ( $terms && ! is_wp_error( $terms ) ){
					foreach ( $terms as $term ) {
						if ($term->name == "Blog"){
							$check = true;
						};
					}
				}
				if ($check == true){
					$typenumber .= "3";
				} else {
					$typenumber .= "2";
				}
			}
			$standby = "blog_item sb";
			$content .= '<div class="item '.$standby.' '.$typenumber.' '.$show.'"><div class="img"></div><div class="content"><a href="'.$resourcelink.'" class="title" '.$target.'>'.get_the_title($resource->ID).'</a>';
			$content .= '<div class="hidden tags">';
			$post_tags = get_the_tags($resource->ID);
			if ( $post_tags ) {
				$tagstring = "";
			    foreach( $post_tags as $tag ) {
			    	$tagstring .= ' '.$tag->slug;
			    }
			    $content .= $tagstring;
			}
			$content .= '</div>';
			$content .= '<p class="by">';
			if (get_post_type($resource->ID) == "post"){
				$content .= '<span class="category">'.get_the_date('m.d.y',$resource->ID).'</span>';
			} else {
				if ( isset ( $resourceinfo['conference_start'] ) ){
			        $startdatearray = explode("-", $resourceinfo['conference_start'][0]);
			    }
			    if ( isset ( $resourceinfo['conference_end'] ) ){
			        $enddatearray = explode("-", $resourceinfo['conference_end'][0]);
			    }
				$content .= '<span class="category">'.$startdatearray[1].'.'.$startdatearray[2].'.'.$startdatearray[0].' - '.$resourceinfo['conference_location'][0].'</span>';
			}
			$content .= '</p></div></div>';
			$count++;
		}
 		$content .= '</div><a href="#" class="more">Load More <i class="fal fa-chevron-circle-down"></i></a>';
		$content .= '</div>';
		
		return $content;
	}
	
	add_shortcode('blog_feed', 'blog_shortcode');
	
	function events_shortcode($atts) {
		$content = '';
		$content .= '<form class="news_search"><h4>Use the search feature below to stay up to date on our latest events.</h4><input type="text" placeholder="Search Events" class="search_box"><button type="submit" class="submit">Search<i class="fas fa-chevron-right"></i></button><div class="filter"><label for="filterby">Filter by:</label><select name="filterby_type" id="filterby"><option value="1">All Tags</option>';
		$tags = get_tags(array(
		  'hide_empty' => false
		));
		foreach ($tags as $tag) {
			$content .= '<option value="'.$tag->slug.'">'.ucwords($tag->name).'</option>';
		}
		$content .= '</select>';
		$terms = get_terms( array(
		    'taxonomy' => 'category',
		    'hide_empty' => false,
		));
		foreach ($terms as $term){
// 			$content .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
		}
		$content .= '</div></form>';
		$content .= '<div class="news_holder"><div class="resources">';
		
		$args = array(
		    'post_type' => 'event',
		    'meta_key' => 'conference_start',
		    'meta_compare' => '>=',
		    'meta_value' => date ('Y-m-d'),
		    'meta_type'  => 'DATE',
		    'post_status' => 'publish',
		    'posts_per_page' => -1,
		    'orderby' => 'meta_value',
		    'order' => 'ASC'
		);
		$resource_list = get_posts( $args );
		$count = 0;
		foreach($resource_list as $resource){
			$show = "activeitem";
			if ($count>=8){
 				$show = "hide";
			}
			$resourceinfo = get_post_meta($resource->ID);
			$resourcelink = get_permalink($resource->ID);
			$target = "";
			if (isset($resourceinfo['resource_file']) && $resourceinfo['resource_file'][0] !== ""){
				$resourcelink = wp_get_attachment_url($resourceinfo['resource_file'][0]);
				$target = 'target="_blank"';
			}
			if (isset($resourceinfo['external_link']) && $resourceinfo['external_link'][0] !== ""){
				$resourcelink = $resourceinfo['external_link'][0];
				$target = 'target="_blank"';
			}
			$thumbnail = "";
			if (get_the_post_thumbnail_url($resource->ID)){
				$thumbnail = 'style="background-image: url('.get_the_post_thumbnail_url($resource->ID).')"';
			}
			$typenumber = "type_";
			if (get_post_type($resource->ID) == "event"){
				$typenumber .= "4";
			} else {
				$terms = get_the_terms($resource->ID, 'category');
				$check = false;
				if ( $terms && ! is_wp_error( $terms ) ){
					foreach ( $terms as $term ) {
						if ($term->name == "Blog"){
							$check = true;
						};
					}
				}
				if ($check == true){
					$typenumber .= "3";
				} else {
					$typenumber .= "2";
				}
			}
			$standby = "events_item sb";
			$content .= '<div class="item '.$standby.' '.$typenumber.' '.$show.'"><div class="img"></div><div class="content"><a href="'.$resourcelink.'" class="title" '.$target.'>'.get_the_title($resource->ID).'</a>';
			$content .= '<div class="hidden tags">';
			$post_tags = get_the_tags($resource->ID);
			if ( $post_tags ) {
				$tagstring = "";
			    foreach( $post_tags as $tag ) {
			    	$tagstring .= ' '.$tag->slug;
			    }
			    $content .= $tagstring;
			}
			$content .= '</div>';
			$content .= '<p class="by">';
			if (get_post_type($resource->ID) == "post"){
				$content .= '<span class="category">'.get_the_date('m.d.y',$resource->ID).'</span>';
			} else {
				if ( isset ( $resourceinfo['conference_start'] ) ){
			        $startdatearray = explode("-", $resourceinfo['conference_start'][0]);
			    }
			    if ( isset ( $resourceinfo['conference_end'] ) ){
			        $enddatearray = explode("-", $resourceinfo['conference_end'][0]);
			    }
				$content .= '<span class="category" style="padding-left: 0;">'.$startdatearray[1].'.'.$startdatearray[2].'.'.$startdatearray[0].' - '.$resourceinfo['conference_location'][0].'</span>';
			}
			$content .= '</p></div></div>';
			$count++;
		}
 		$content .= '</div>';
 		if ($count > 6){
 			$content .= '<a href="#" class="more">Load More <i class="fal fa-chevron-circle-down"></i></a>';
 		}
		$content .= '</div>';
		
		return $content;
	}
	
	add_shortcode('events_feed', 'events_shortcode');
	
	function my_beaver_builder_column_class( $class, $col ) {

	  // Column width class
	
	    switch ( $col->settings->size ) {
	
	      case '100':
	        $class .= ' fl-col-width-1-1';
	      break;
	
	      case '50':
	      case '50.01':
	        $class .= ' fl-col-width-1-2';
	      break;
	
	      case '33.33':
	      case '33.34':
	        $class .= ' fl-col-width-1-3';
	      break;
	
	      case '66.66':
	        $class .= ' fl-col-width-2-3';
	      break;
	
	      case '25':
	      case '24.96':
	      case '25.03':
	        $class .= ' fl-col-width-1-4';
	      break;
	
	      case '75':
	        $class .= ' fl-col-width-3-4';
	      break;
	      
	      case '37':
	        $class .= ' fl-col-width-callout-1';
	      break;
	      
	      case '2':
	        $class .= ' fl-col-width-callout-2';
	      break;
	      
	      case '61':
	        $class .= ' fl-col-width-callout-3';
	      break;
	
	      case '20':
	        $class .= ' fl-col-width-1-5';
	      break;
	
	      case '16.65':
	      case '16.66':
	        $class .= ' fl-col-width-1-6';
	      break;
	      
	      case '25.01':
	      	$class .= ' fl-col-sidebars-1';
	      break;
	      
	      case '49.99':
	      	$class .= ' fl-col-sidebars-2';
	      break;
	
	      default:
	        $class .= ' fl-col-width-custom';
	      break;
	
	    }
	
	  // Output
	
	    return $class;
	
	} // /my_beaver_builder_column_class
	
	add_filter( 'fl_builder_column_custom_class', 'my_beaver_builder_column_class', 10, 2 );
	
	function prfx_meta_save( $post_id ) {
		if( isset( $_POST[ 'redirect' ] ) ) { 
			$redirect = $_POST['redirect'];
		    update_post_meta($post_id,'redirect',$redirect); 
		}
		if( isset( $_POST[ 'technique' ] ) ) { 
			$technique = $_POST['technique'];
		    update_post_meta($post_id,'technique',$technique); 
		}
		if( isset( $_POST[ 'species-matrix' ] ) ) { 
			$speciesmatrix = $_POST['species-matrix'];
		    update_post_meta($post_id,'species-matrix',$speciesmatrix); 
		}
		if( isset( $_POST[ 'validated' ] ) ) { 
			$validated = $_POST['validated'];
		    update_post_meta($post_id,'validated',$validated); 
		}
		if( isset( $_POST[ 'biomarkerassays' ] ) ) { 
			$biomarkerassays = $_POST['biomarkerassays'];
		    update_post_meta($post_id,'biomarkerassays',$biomarkerassays); 
		}
		if( isset( $_POST[ 'pkassays' ] ) ) { 
			$pkassays = $_POST['pkassays'];
		    update_post_meta($post_id,'pkassays',$pkassays); 
		}
		if( isset( $_POST[ 'adaassays' ] ) ) { 
			$adaassays = $_POST['adaassays'];
		    update_post_meta($post_id,'adaassays',$adaassays); 
		}
		if( isset( $_POST[ 'range' ] ) ) { 
			$range = $_POST['range'];
		    update_post_meta($post_id,'range',$range); 
		}
		if( isset( $_POST[ 'anti-coag' ] ) ) { 
			$anticoag = $_POST['anti-coag'];
		    update_post_meta($post_id,'anti-coag',$anticoag); 
		}
		if( isset( $_POST[ 'keywords' ] ) ) { 
			$keywords = $_POST['keywords'];
		    update_post_meta($post_id,'keywords',$keywords); 
		}
		if( isset( $_POST[ 'external_link' ] ) ) { 
			$external_link = $_POST['external_link'];
		    update_post_meta($post_id,'external_link',$external_link); 
		}
		if( isset( $_POST[ 'resource_file' ] ) ) { 
			$resource_file = $_POST['resource_file'];
		    update_post_meta($post_id,'resource_file',$resource_file); 
		}
		if( isset( $_POST[ 'conference_start' ] ) ) { 
			$conference_start = $_POST['conference_start'];
		    update_post_meta($post_id,'conference_start',$conference_start); 
		}
		if( isset( $_POST[ 'conference_end' ] ) ) { 
			$conference_end = $_POST['conference_end'];
		    update_post_meta($post_id,'conference_end',$conference_end); 
		}
		if( isset( $_POST[ 'conference_location' ] ) ) { 
			$conference_location = $_POST['conference_location'];
		    update_post_meta($post_id,'conference_location',$conference_location); 
		}
		if( isset( $_POST[ 'conference_weblink' ] ) ) { 
			$conference_weblink = $_POST['conference_weblink'];
		    update_post_meta($post_id,'conference_weblink',$conference_weblink); 
		}
		if( isset( $_POST[ 'conference_email' ] ) ) { 
			$conference_email = $_POST['conference_email'];
		    update_post_meta($post_id,'conference_email',$conference_email); 
		}
	}
	add_action( 'save_post', 'prfx_meta_save' );
	
	function my_mce_buttons_2( $buttons ) {	
		/**
		 * Add in a core button that's disabled by default
		 */
		$buttons[] = 'superscript';
		$buttons[] = 'subscript';
	
		return $buttons;
	}
	add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );
	
	@ini_set( 'upload_max_size' , '64M' );
	@ini_set( 'post_max_size', '64M');
	@ini_set( 'max_execution_time', '300' );